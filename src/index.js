import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";
import "./css/index.css";
import "argon-design-system-free/assets/vendor/headroom/headroom.min.js";
import "argon-design-system-free/assets/js/argon.js?v=1.1.0";
import "./css/argon-dashboard.min.css";
import "./css/argon.min.css";
import Login from "./components/Login/Login";
import Register from "./components/Register/Register";
import Home from "./components/Home/Home";
import * as serviceWorker from "./serviceWorker";
import NewLoginPage from "./components/NewLogin/NewLogin";
import VerificationPage from "./components/Verification/Verification";
import ConfirmEmail from "./components/ConfirmEmail/ComfirmEmail";
import ForgotPassword from "./components/ForgotPassword/Forgotpasssword";
import ChangePassword from "./components/ChangePassword/ChangePassword";
ReactDOM.render(
  <Router>
    <React.Fragment>
      <Route exact path="/login" component={Login} />
      <Route exact path="/verify-email" component={VerificationPage} />
      <Route exact path="/confirm-email/:token" component={ConfirmEmail} />
      <Route exact path="/forgotpassword" component={ForgotPassword} />
      <Route exact path="/reset-password/:token" component={ChangePassword} />
      <Route exact path="/login-new" component={NewLoginPage} />
      <Route exact path="/register" component={Register} />
      <PrivateRoute exact path="/wallet" component={Home} />
      <PrivateRoute exact path="/records" component={Home} />
      <PrivateRoute exact path="/my-patients" component={Home} />
      <PrivateRoute exact path="/my-records" component={Home} />
      <PrivateRoute exact path="/my-team" component={Home} />
      <PrivateRoute exact path="/bids" component={Home} />
      <PrivateRoute exact path="/buyerbids" component={Home} />
      <PrivateRoute exact path="/" component={Home} />
      <PrivateRoute exact path="/dashboard" component={Home} />
      <PrivateRoute exact path="/teams" component={Home} />
      <PrivateRoute exact path="/add" component={Home} />
      <PrivateRoute path="/view/:id" component={Home} />
      <PrivateRoute path="/team/:id" component={Home} />
      <PrivateRoute path="/records-for-bid/:id" component={Home} />
      <PrivateRoute path="/all-bids" component={Home} />
      {/* <PrivateRoute path="/edit/:id" component={EditRecord} /> */}
    </React.Fragment>
  </Router>,
  document.getElementById("root")
);

function PrivateRoute({ component: Component, ...rest }) {
  return (
    <Route
      {...rest}
      render={props =>
        localStorage.getItem("token") ? (
          <Component {...props} />
        ) : (
            <Redirect
              to={{ pathname: "/login", state: { from: props.location } }}
            />
          )
      }
    />
  );
}

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

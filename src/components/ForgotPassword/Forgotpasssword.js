import React, { Component } from "react";
import Icon from "@mdi/react";
import { mdiEmail, mdiInstagram, mdiFacebook, mdiTwitter } from "@mdi/js";
import axios from "axios";
import ReactLoading from "react-loading";
import { API_URL } from "../../common";
import {
  ToastsContainer,
  ToastsStore,
  ToastsContainerPosition,
} from "react-toasts";

var logo = require("../../img/logo.png");

class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      loading: false,
    };
  }

  componentDidMount() {
    document.title = "Forgot Password";
    document.body.classList.add("black");
  }

  componentWillUnmount() {
    document.body.classList.remove("black");
  }

  handleSubmit = (event) => {
    event.preventDefault();
    var self = this;

    if (this.state.email === "") {
      ToastsStore.warning("please enter email");
    } else {
      self.setState({ loading: true });
      var url = API_URL + "user/forgotPassword";

      var payload = {
        email: this.state.email,
      };

      axios
        .post(url, payload)
        .then(function (response) {
          if (response.data.success) {
            ToastsStore.error(
              "Password recovery email is sent please check your email."
            );
            self.props.history.push("/login");
          } else {
            self.setState({ loading: false });
            ToastsStore.error(response.data.message);
          }
        })
        .catch(function (error) {
          self.setState({ loading: false });
          ToastsStore.error(error.response.data.message);
        });
    }
  };

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {
    return this.state.isLoading ? (
      <div className="centered">
        <ReactLoading
          type="spin"
          color="#2B70A0"
          height={"64px"}
          width={"64px"}
        />
      </div>
    ) : (
      <React.Fragment>
        <ToastsContainer
          store={ToastsStore}
          position={ToastsContainerPosition.TOP_RIGHT}
        />
        <main>
          <section className="section section-shaped section-lg">
            <div className="shape shape-style-1 bg-gradient-default">
              <span />
              <span />
              <span />
              <span />
              <span />
              <span />
              <span />
              <span />
            </div>
            <div className="container">
              <div className="row justify-content-center">
                <div className="col-lg-5">
                  <div className="card bg-secondary shadow border-0">
                    <div className="card-header bg-white text-center">
                      <img src={logo} height="80px" alt="Logo" />
                    </div>
                    <div className="card-body px-lg-5 py-lg-2">
                      <div className="text-center text-muted mb-4">
                        <small>Enter registered email</small>
                      </div>
                      <form htmlFor="form" onSubmit={this.handleSubmit}>
                        <div className="form-group mb-3">
                          <div className="input-group input-group-alternative">
                            <div className="input-group-prepend">
                              <span className="input-group-text">
                                <Icon
                                  path={mdiEmail}
                                  title="Password recovery"
                                  size={1}
                                  horizontal
                                  vertical
                                  rotate={180}
                                  color="#9e9e9e"
                                />
                              </span>
                            </div>
                            <input
                              className="form-control"
                              placeholder="Email"
                              type="email"
                              name="email"
                              value={this.state.email}
                              onChange={this.handleChange}
                              required
                            />
                          </div>
                        </div>
                        <div className="text-center">
                          <button
                            type="submit"
                            className="btn btn-primary my-4"
                          >
                            Send
                          </button>
                        </div>
                      </form>
                    </div>
                  </div>
                  <div className="row mt-3">
                    <div className="col text-right">
                      <a href="/login" className="text-light">
                        <small>Login</small>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </main>
        <footer className="footer">
          <div className="container">
            <div className="row row-grid align-items-center mb-5">
              <div className="col-lg-6">
                <h3 className="text-primary font-weight-light mb-2">
                  Thank you for supporting us!
                </h3>
                <h4 className="mb-0 font-weight-light">
                  Let's get in touch on any of these platforms.
                </h4>
              </div>
              <div className="col-lg-6 text-lg-center btn-wrapper">
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href="https://twitter.com/csa"
                  className="btn btn-neutral btn-icon-only btn-twitter btn-round btn-lg"
                  data-toggle="tooltip"
                  data-original-title="Follow us"
                >
                  <Icon
                    path={mdiTwitter}
                    title="User Profile"
                    size={1}
                    horizontal
                    vertical
                    rotate={180}
                    color="#fff"
                  />
                </a>
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href="https://www.facebook.com/csa"
                  className="btn btn-neutral btn-icon-only btn-facebook btn-round btn-lg"
                  data-toggle="tooltip"
                  data-original-title="Like us"
                >
                  <Icon
                    path={mdiFacebook}
                    title="User Profile"
                    size={1}
                    horizontal
                    vertical
                    rotate={180}
                    color="#fff"
                  />
                </a>
                <a
                  target="_blank"
                  href="/"
                  className="btn btn-neutral btn-icon-only btn-dribbble btn-lg btn-round"
                  data-toggle="tooltip"
                  data-original-title="Follow us"
                >
                  <Icon
                    path={mdiInstagram}
                    title="User Profile"
                    size={1}
                    horizontal
                    vertical
                    rotate={180}
                    color="#fff"
                  />
                </a>
              </div>
            </div>
            <hr />
            <div className="row align-items-center justify-content-md-between">
              <div className="col-md-6">
                <div className="copyright">
                  &copy; 2020
                  <a href="/" target="_blank">
                    MyContext
                  </a>
                  .
                </div>
              </div>
              <div className="col-md-6">
                <ul className="nav nav-footer justify-content-end">
                  <li className="nav-item">
                    <a href="/" className="nav-link" target="_blank">
                      MyContext
                    </a>
                  </li>
                  <li className="nav-item">
                    <a href="/" className="nav-link" target="_blank">
                      About Us
                    </a>
                  </li>
                  <li className="nav-item">
                    <a href="/" className="nav-link" target="_blank">
                      License
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </footer>
      </React.Fragment>
    );
  }
}

export default ForgotPassword;

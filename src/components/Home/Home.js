import React, { Component } from "react";
import "./Home.css";
import SideNav from "../SideNav/SideNav";
import NavBar from "../NavBar/NavBar";
import Footer from "../Footer/Footer";
import axios from "axios";
import { ToastsStore } from "react-toasts";

import { API_URL } from "../../common";
import Dashboard from "../Dashboard/Dashboard";
import Records from "../Records/Records";
import MyRecords from "../MyRecord/MyRecords";
import ViewRecord from "../ViewRecord/ViewRecord";
import MyPatients from "../MyPatients/MyPatients";
import MyTeam from "../MyTeam/MyTeam";
import Graphs from "../Graph/Graphs";
import Teams from "../Teams/Teams";
import Team from "../Team/Team";
import Bids from "../Bids/Bids";
import BuyerBids from "../BuyerBids/BuyerBids";
import RecordsForBids from "../Records/RecordsForBids";
import AllBids from "../Bids/AllBids";
import $ from "jquery";
import Wallet from "../Wallet/Wallet";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loading: true,
      paymentDialog: {
        isOpen: false,
        selectedRecordId: undefined,
      },
    };
  }

  componentDidMount() {
    let pathName = window.location.pathname;
    let component = <Dashboard />;
    let header = undefined;
    pathName = pathName.slice(1);
    switch (pathName) {
      case "":
      case "dashboard":
        component = <Dashboard />;
        header = <Graphs />;
        pathName = "dashboard";
        break;
      case "records":
        component = <Records />;
        pathName = "records";
        break;
        case "wallet":
        component = <Wallet />;
        pathName = "wallet";
        break;
      case "my-patients":
        component = <MyPatients />;
        pathName = "my-patients";
        break;
      case "all-bids":
        component = <AllBids />;
        pathName = "all-bids";
        break;
      case "my-team":
        component = <MyTeam />;
        pathName = "my-team";
        break;
      case "bids":
        component = <Bids />;
        pathName = "bids";
        break;
      case "buyerbids":
        component = <BuyerBids />;
        pathName = "buyerbids";
        break;
      case "teams":
        component = <Teams />;
        break;
      case "my-records":
        component = <MyRecords />;
        pathName = "my-records";
        break;
      default:
        if (pathName.indexOf("view") !== -1) {
          component = <ViewRecord recordId={this.props.match.params.id} />;
          pathName = "view";
        }
        else if(pathName.indexOf("records-for-bid") !==-1)
        {
          component = <RecordsForBids bidId={this.props.match.params.id} />;
          pathName = "records-for-bid"
        }
        else {
          component = <Team teamId={this.props.match.params.id} />;
          pathName = "team";
        }
        break;
    }

    this.setState({
      component,
      header,
      title: pathName,
    });
  }

  componentWillUnmount() {
    document.body.classList.remove("white");
  }

  handlePaymentDialogClose = (data) => {
    if (data) {
      let self = this;
      self.setState({ loading: true });

      var url = API_URL + "user/buy-record";
      data.userId = localStorage.getItem("user_id");
      data.recordId = self.state.paymentDialog.selectedRecordId;
      axios
        .post(url, data, {
          headers: {
            "x-access-token": localStorage.getItem("access-token"),
          },
        })
        .then(function (response) {
          if (response.data.success) {
            self.setState({
              paymentDialog: {
                isOpen: false,
                selectedRecordId: undefined,
              },
              loading: false,
            });
            ToastsStore.success("Payment Done.");
          } else {
            self.setState({ loading: false });
            ToastsStore.error(response.data.message);
          }
        })
        .catch(function (error) {
          self.setState({ loading: false });
          ToastsStore.error(error.response.data.message);
        });
    } else {
      this.setState({
        paymentDialog: {
          isOpen: false,
          selectedRecordId: undefined,
        },
      });
    }
  };

  handlePaymentDialogOpen = (recordId) => {
    this.setState(
      {
        paymentDialog: {
          isOpen: true,
          selectedRecordId: recordId,
        },
      },
      () => {
        $("#paymentDialog").modal("show");
      }
    );
  };

  handleNavigation = (url) => {
    this.props.history.push("/" + url);
  };

  getComponent = (component, type, header) => {
    this.setState({ component: component, title: type, header: header });
  };

  getLogout = () => {
    this.props.history.push("/");
  };

  getBody = () => {
    let pathName = window.location.pathname;
    pathName = pathName.slice(1);

    switch (pathName) {
      case "":
      case "dashboard":
        return <Dashboard />;
      case "records":
        return <Records />;
        case "wallet":
        return <Wallet />;
      case "my-patients":
        return <MyPatients />;
      case "teams":
        return <Teams />;
      case "my-records":
        return <MyRecords />;
      case "my-team":
        return <MyTeam />;
      case "all-bids":
        return <AllBids />;
      case "bids":
        return <Bids />;
      case "buyerbids":
        return <BuyerBids />;
      default:
        if (pathName.indexOf("view") !== -1) {
          return <ViewRecord recordId={this.props.match.params.id} />;
        }
        else if(pathName.indexOf("records-for-bid") !==-1)
        {
          return <RecordsForBids bidId={this.props.match.params.id} />;
        }
        else {
          return <Team teamId={this.props.match.params.id} />;
        }
    }
  };

  getHeader = () => {
    let pathName = window.location.pathname;
    pathName = pathName.slice(1);
    switch (pathName) {
      default:
      case "dashboard":
        return <Graphs />;
    }
  };

  render() {
    return (
      <React.Fragment>
        <SideNav
          title={this.state.title}
          setComponent={this.getComponent}
          handleNavigation={this.handleNavigation}
          setLogout={this.getLogout}
        />
        <div className="main-content">
          <NavBar title={this.state.title} setLogout={this.getLogout} />
          <div className="header bg-gradient-primary pb-8 pt-5 pt-md-8">
            <div className="container-fluid mt-75">
              <div className="header-body">
                {this.state.header ? this.getHeader() : ""}
              </div>
            </div>
          </div>
          <div className="container-fluid">
            <div className="row mt-64">
              <div className="col">
                <div className="card shadow">
                  <div className="card-body" style={{ minHeight: "200px" }}>
                    {this.getBody()}
                  </div>
                </div>
              </div>
            </div>

            <Footer />
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Home;

import React, { Component } from "react";
import axios from "axios";
import {
  ToastsContainer,
  ToastsStore,
  ToastsContainerPosition,
} from "react-toasts";
import { API_URL } from "../../common";
import Record from "./Record";

class ViewRecord extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: undefined,
      loading: true,
    };
  }

  componentDidMount() {
    document.title = "Medical Record";
    document.body.classList.add("white");
    var self = this;

    var url = API_URL + "record/viewRecord";

    var payload = {
      token: localStorage.getItem("token"),
      id: this.props.recordId,
    };

    axios
      .post(url, payload)
      .then(function (response) {
        if (response.data.success) {
          self.setState({ data: response.data.data, loading: false });
          console.log(self.state.data);
        } else {
          ToastsStore.error(response.data.message);
        }
      })
      .catch(function (error) {
        ToastsStore.error(error.response.data.message);
      });
  }

  componentWillUnmount() {
    document.body.classList.remove("white");
  }

  render() {
    return (
      <React.Fragment>
        <ToastsContainer
          store={ToastsStore}
          position={ToastsContainerPosition.TOP_RIGHT}
        />

        <h3>
          {this.state.data
            ? this.state.data.patient.name + " Medical Record"
            : ""}
        </h3>
        <Record data={this.state.data} />
      </React.Fragment>
    );
  }
}

export default ViewRecord;

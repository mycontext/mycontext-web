import React, { Component } from "react";
import Moment from "moment";

class Record extends Component {
  constructor(props) {
    super(props);
    this.props = {
    };
  }

  componentDidMount(){
    Moment.locale("en");
  }

  render() {
    return (
      <React.Fragment>
        {this.props.data ? (
          <React.Fragment>
            <table className="table table-striped table-bordered table-set">
              <tbody>
                <tr>
                  <th>Blood Group</th>
                  <td>{this.props.data.blood_group}</td>
                </tr>
                <tr>
                  <th>Blood Pressure</th>
                  <td>{this.props.data.bpressure}</td>
                </tr>
                <tr>
                  <th>Cancer Type</th>
                  <td>{this.props.data.cancer_type}</td>
                </tr>
                <tr>
                  <th>Intial Tumor Size</th>
                  <td>{this.props.data.cs_tumor_size}</td>
                </tr>
                <tr>
                  <th>Weight</th>
                  <td>{this.props.data.cweight}</td>
                </tr> <tr>
                  <th>Date of Diagnosis</th>
                  <td>{Moment(this.props.data.date_of_diagnosis).format("LL")}</td>
                </tr>
                <tr>
                  <th>Refered By</th>
                  <td>{this.props.data.doctor.name}</td>
                </tr>
                <tr>
                  <th>Gender</th>
                  <td>{this.props.data.gender}</td>
                </tr>
                <tr>
                  <th>Outcome Date</th>
                  <td>{Moment(this.props.data.outcome_date).format("LL")}</td>
                </tr>
                <tr>
                  <th>Year Of Birth</th>
                  <td>{this.props.data.year_of_birth}</td>
                </tr>
                <tr>
                  <th>Created On</th>
                  <td>{Moment(this.props.data.createdAt).format("LL")}</td>
                </tr>
              </tbody>
            </table>
          </React.Fragment>
        ) : (
          <p>No medical records found.</p>
        )}
      </React.Fragment>
    );
  }
}

export default Record;

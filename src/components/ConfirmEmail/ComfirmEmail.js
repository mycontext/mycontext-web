import React, { Component } from 'react';
import { RingLoader } from 'react-spinners';
import axios from 'axios';
import { ToastsStore, ToastsContainer, ToastsContainerPosition } from 'react-toasts';
import { API_URL } from '../../common';
class ConfirmEmail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            message: "",
        }
    }
    componentDidMount() {
        this.verifyAccount();
    }
    verifyAccount = () => {
        let token = window.location.href.split("/").pop()
        let url = API_URL + "user/confirmation/" + token;
        let self = this;
        debugger;
        axios
            .get(url)
            .then(function (response) {
                debugger
                if (response.data.success) {
                    ToastsStore.success("Verification successful, login to continue..");
                    setTimeout(()=>{
                        self.props.history.push("/login");
                    },2000);
                } else {
                    self.setState({ isLoading: false, message: response.data.message });
                    ToastsStore.error(response.data.message);
                }
            })
            .catch(function (error) {
                self.setState({ isisLoading: false });
                ToastsStore.error(error.response.data.message);
            });
    }
    render() {
        return (
            <section className="padding-16">
                {this.state.isLoading && (
                    <div className="loading">
                        <RingLoader
                            sizeUnit={"px"}
                            size={80}
                            color={"#0ca678"}
                        />
                    </div>
                )}
                <ToastsContainer
                    store={ToastsStore}
                    position={ToastsContainerPosition.TOP_RIGHT}
                />
                <div className="row">
                    <div className="col-sm-9 col-md-7 col-lg-5 mx-auto mtb">
                        <br />
                        <br />
                        <div className="card card-signin">
                            <div className="card-body text-center">
                                <img src="/images/email.png" className="mb-4" alt="email" />
                                <h5 className="m-0 text-center">
                                    {this.state.isLoading ? "Verifying Your account Please wait" : this.state.message ? this.state.message : "Some thing went wrong"}
                                </h5>
                            </div>
                        </div>
                    </div>
                </div>

            </section>
        );
    }
}

export default ConfirmEmail;
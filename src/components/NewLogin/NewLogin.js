import React, { Component } from "react";
import { RingLoader } from "react-spinners";
import {
  ToastsStore,
  ToastsContainer,
  ToastsContainerPosition
} from "react-toasts";
import axios from "axios";
import { API_URL } from "../../common";
class NewLogin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      login: {
        email: "",
        password: ""
      },
      register: {
        name: "",
        email: "",
        password: "",
        confirmPassword: "",
        user_type: "Patient",
        loading: false
      }
    };
  }
  componentDidMount() {
    document.title = "My Context";
  }
  handleLoginChange = (event, value) => {
    let eve = { ...event };
    this.setState(prevState => ({
      login: {
        ...prevState.login,
        [eve.target.name]: value ? value : eve.target.value
      }
    }));
  };
  handleRegisterChange = (event, value) => {
    let eve = { ...event };
    this.setState(prevState => ({
      register: {
        ...prevState.register,
        [eve.target.name]: value ? value : eve.target.value
      }
    }));
  };
  handleSignIn = event => {
    event.preventDefault();
    var self = this;
    self.setState({ isLoading: true });

    var url = API_URL + "user/login";

    var payload = { ...this.state.login };

    axios
      .post(url, payload)
      .then(function(response) {
        if (response.data.success) {
          localStorage.setItem("access-token", response.data.token);
          localStorage.setItem("name", response.data.name);
          localStorage.setItem("user_id", response.data.user_id);
          localStorage.setItem("email", response.data.email);
          localStorage.setItem("user_type", response.data.user_type);
          self.props.history.push("/");
        } else {
          self.setState({ isLoading: false });
          ToastsStore.error(response.data.message);
        }
      })
      .catch(function(error) {
        self.setState({ isLoading: false });
        ToastsStore.error(error.response.data.message);
      });
  };
  handleRegister = event => {
    event.preventDefault();
    var self = this;

    if (this.state.register.password !== this.state.register.confirmPassword) {
      ToastsStore.warning("Password doesn't match");
    } else {
      self.setState({ isLoading: true });

      var url = API_URL + "user/register";

      var payload = { ...this.state.register };

      payload.uiUrl = window.location.origin;

      axios
        .post(url, payload)
        .then(function(response) {
          if (response.data.success) {
            ToastsStore.success(
              "Registration successful, please verify email to continue.."
            );
            self.props.history.push("/verify-email");
          } else {
            self.setState({ isLoading: false });
            ToastsStore.error(response.data.message);
          }
        })
        .catch(function(error) {
          self.setState({ isisLoading: false });
          ToastsStore.error(error.response.data.message);
        });
    }
  };
  render() {
    return (
      <React.Fragment>
        {this.state.isLoading && (
          <div className="loading">
            <RingLoader sizeUnit={"px"} size={80} color={"#0ca678"} />
          </div>
        )}
        <ToastsContainer
          store={ToastsStore}
          position={ToastsContainerPosition.TOP_RIGHT}
        />
        <section className="footer">
          <div className="row">
            <div className="col d-flex align-items-center">
              <h2 className="text-white">My Context</h2>
            </div>
            <div className="col">
              <form className="form-signin" onSubmit={this.handleSignIn}>
                <table align="right" className="mr-4" cellPadding="4">
                  <thead>
                    <tr>
                      <th className="text-white">Username</th>
                      <th className="text-white">Password</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>
                        <input
                          type="email"
                          id="inputEmail"
                          className="form-control"
                          placeholder="Email address"
                          name="email"
                          value={this.state.login.email}
                          onChange={this.handleLoginChange}
                          required
                          autoFocus
                        />
                      </td>
                      <td>
                        <input
                          type="password"
                          id="inputPassword"
                          className="form-control"
                          placeholder="Password"
                          name="password"
                          value={this.state.login.password}
                          onChange={this.handleLoginChange}
                          required
                        />
                      </td>
                    </tr>
                    <tr>
                      <td colSpan="2" className="text-right">
                        <button className="btn text-white green-bg">
                          Sign In
                        </button>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </form>
            </div>
          </div>
        </section>
        <main role="main" className="padding-16 container mt-4">
          <div className="row">
            <div className="col text-center">
              {/* <div className="text-center">
                                <i className="fas fa-circle fa-10x green"></i>
                            </div> */}
              <div className="d-flex flex-column h-100 justify-content-center">
                <div>
                  <i className="fas fa-stethoscope fa-7x mt-4"></i>
                </div>
                <br />
                <br />
                <div className="d-flex justify-content-around">
                  <i className="fas fa-chart-pie fa-7x mt-4 text-left"></i>

                  <i className="fas fa-file-alt fa-7x mt-4 text-right"></i>
                </div>
              </div>
            </div>

            <div className="col">
              <form className="form-signin" onSubmit={this.handleRegister}>
                <table className="border" align="center" cellPadding="10">
                  <tbody>
                    <tr>
                      <td colSpan="2">
                        <h5 className="m-0 text-light">
                          New User? Register with us now!
                        </h5>
                      </td>
                    </tr>
                    <tr>
                      <td className="text-light">Email</td>
                      <td>
                        <input
                          type="email"
                          id="inputEmail"
                          className="form-control"
                          placeholder="Email address"
                          name="email"
                          value={this.state.register.email}
                          onChange={this.handleRegisterChange}
                          required
                        />
                      </td>
                    </tr>
                    <tr>
                      <td className="text-light">Username</td>
                      <td>
                        <input
                          type="text"
                          id="inputName"
                          className="form-control"
                          placeholder="Name"
                          name="name"
                          value={this.state.register.name}
                          onChange={this.handleRegisterChange}
                          required
                        />
                      </td>
                    </tr>
                    <tr>
                      <td className="text-light">Password</td>
                      <td>
                        <input
                          type="password"
                          id="inputPassword"
                          className="form-control"
                          placeholder="Password"
                          name="password"
                          value={this.state.register.password}
                          onChange={this.handleRegisterChange}
                          required
                        />
                      </td>
                    </tr>
                    <tr>
                      <td className="text-light">Confirm Password</td>
                      <td>
                        <input
                          type="password"
                          id="inputConfirmPassword"
                          className="form-control"
                          placeholder="Confirm Password"
                          name="confirmPassword"
                          value={this.state.register.confirmPassword}
                          onChange={this.handleRegisterChange}
                          required
                        />
                      </td>
                    </tr>
                    <tr className="text-light">
                      <td colSpan="2">Select User Type:</td>
                    </tr>
                    <tr>
                      <td colSpan="2" className="text-light">
                        <div className="mt--10">
                          <label className="mr-2">
                            <input
                              name="user_type"
                              type="radio"
                              value="Patient"
                              onChange={this.handleRegisterChange}
                              checked={
                                this.state.register.user_type === "Patient"
                              }
                            />{" "}
                            Patient
                          </label>
                          <label className="mr-2">
                            <input
                              name="user_type"
                              type="radio"
                              value="Doctor"
                              onChange={this.handleRegisterChange}
                              checked={
                                this.state.register.user_type === "Doctor"
                              }
                            />{" "}
                            Doctor
                          </label>
                          <label className="mr-2">
                            <input
                              name="user_type"
                              type="radio"
                              value="Hospital"
                              onChange={this.handleRegisterChange}
                              checked={
                                this.state.register.user_type === "Hospital"
                              }
                            />{" "}
                            Hospital
                          </label>
                          <label className="mr-2">
                            <input
                              name="user_type"
                              type="radio"
                              value="Pathologist"
                              onChange={this.handleRegisterChange}
                              checked={
                                this.state.register.user_type === "Pathologist"
                              }
                            />{" "}
                            Pathologist
                          </label>
                          <label className="mr-2">
                            <input
                              name="user_type"
                              type="radio"
                              value="Pharmaceutical Company"
                              onChange={this.handleRegisterChange}
                              checked={
                                this.state.register.user_type ===
                                "Pharmaceutical Company"
                              }
                            />{" "}
                            Pharmaceutical Company
                          </label>
                          <label className="mr-2">
                            <input
                              name="user_type"
                              type="radio"
                              value="Pathology Laboratory"
                              onChange={this.handleRegisterChange}
                              checked={
                                this.state.register.user_type ===
                                "Pathology Laboratory"
                              }
                            />{" "}
                            Pathology Laboratory
                          </label>
                          <label className="mr-2">
                            <input
                              name="user_type"
                              type="radio"
                              value="Insurance Company"
                              onChange={this.handleRegisterChange}
                              checked={
                                this.state.register.user_type ===
                                "Insurance Company"
                              }
                            />{" "}
                            Insurance Company
                          </label>
                        </div>
                      </td>
                    </tr>

                    <tr className="text-light text-right">
                      <td colSpan="2">
                        <button className="btn btn-light">Sign Up</button>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </form>
            </div>
          </div>
        </main>
      </React.Fragment>
    );
  }
}

export default NewLogin;

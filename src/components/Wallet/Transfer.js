import React, { Component } from "react";
import axios from "axios";
import {
  ToastsContainer,
  ToastsStore,
  ToastsContainerPosition,
} from "react-toasts";
import { API_URL } from "../../common";

class Transfer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: undefined,
      share: ""
    };
  }

  componentDidMount() {}

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleVerification = (event) => {
    event.preventDefault();
    var self = this;

    self.setState({ loading: true });

    var payload = {
      token: localStorage.getItem("token"),
      accountNumber: this.state.accountNumber,
      accountHolderName:this.state.accountHolderName,
      amount: this.state.amount,
      bsbNumber:this.state.bsbNumber
    };

    var url = API_URL + "wallet/getVerificationCode";

    axios
      .post(url, payload)
      .then(function (response) {
        console.log(response);
        if (response.data.success) {
          window.location.reload(); 
          ToastsStore.success(response.data.message);
        } else {
          self.setState({ loading: false });
          ToastsStore.error(response.data.message); 
        }
      })
      .catch(function (error) {
        console.log(error);
        
        self.setState({ loading: false });         
        ToastsStore.error("Unable to complete request");
        console.log(error);
      });
  };
  handleSubmit = (event) => {
    event.preventDefault();
    var self = this;

    self.setState({ loading: true });

    var payload = {
      token: localStorage.getItem("token"),
      accountNumber: this.state.accountNumber,
      accountHolderName:this.state.accountHolderName,
      amount: this.state.amount,
      bsbNumber:this.state.bsbNumber,
      verificationCode:this.state.verificationCode
    };

    var url = API_URL + "wallet/transferMoney";

    axios
      .post(url, payload)
      .then(function (response) {
        if (response.data.success) {
          window.location.reload();
          ToastsStore.success(response.data.message);
        } else {
          self.setState({ loading: false });
          ToastsStore.error(response.data.message);
        }
      })
      .catch(function (error) {
        self.setState({ loading: false });
        ToastsStore.error(error.response.data.message);
        console.log(error);
      });
  };
  render() {
    return (
      <React.Fragment>
        <ToastsContainer
          store={ToastsStore}
          position={ToastsContainerPosition.TOP_RIGHT}
        />
        <div className="">
          <button
            type="button"
            className="btn btn-danger mb-4 pull-left"
            data-toggle="modal"
            data-target="#createModal"
          >
            Make a Transfer
          </button>
        </div>
        <div
          className="modal fade"
          id="createModal"
          tabindex="-1"
          role="dialog"
          aria-labelledby="createModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="createModalLabel">
                  Account Balance:<strong> {this.props.walletAmount}</strong>
                </h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                <form className="form-signin" onSubmit={this.handleSubmit}>
                  <div className="container-add">
                  <div className="form-group">
                      <input
                        type="accountHolderName"
                        className="form-control form-control-alternative"
                        id="accountHolderName"
                        name="accountHolderName"
                        value={this.state.accountHolderName}
                        onChange={this.handleChange}
                        placeholder="Account Name"
                        required
                      />
                    </div>
                    <div className="form-group">
                      <input
                        type="accountNumber"
                        className="form-control form-control-alternative"
                        id="accountNumber"
                        name="accountNumber"
                        value={this.state.accountNumber}
                        onChange={this.handleChange}
                        placeholder="Account Number"
                        required
                      />
                    </div>
                    
                    <div className="form-group">
                      <input
                        type="bsbNumber"
                        className="form-control form-control-alternative"
                        id="bsbNumber"
                        name="bsbNumber"
                        value={this.state.bsbNumber}
                        onChange={this.handleChange}
                        placeholder="BSB Number"
                        required
                      />
                    </div>
                    <div className="form-group">
                      <input
                        type="number"
                        min="0"
                        max={this.props.walletAmount}
                        className="form-control form-control-alternative"
                        id="amount"
                        name="amount"
                        value={this.state.amount}
                        onChange={this.handleChange}
                        placeholder="amount"
                        required
                      />
                    </div>   
                    <div className="form-group">
                    <div> Send one time code for Transfer</div>
                    <button type="button" className="btn btn-success" onClick={this.handleVerification}>
                      Send code
                    </button>
                      </div> 
                    <div className="form-group">
                      <input
                        type="verificationCode"
                        className="form-control form-control-alternative"
                        id="verificationCode"
                        name="verificationCode"
                        value={this.state.verificationCode}
                        onChange={this.handleChange}
                        placeholder="Verification Code"
                        required
                      />
                    </div>
                  </div>
                  <div className="modal-footer">
                    <button
                      type="button"
                      className="btn btn-secondary"
                      data-dismiss="modal"
                    >
                      Close
                    </button>
                    <button type="submit" className="btn btn-primary">
                      Transfer
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Transfer;

import React, { Component } from "react";
import axios from "axios";
// import { ToastsStore } from "react-toasts";
// import $ from "jquery";
import { API_URL } from "../../common";
import { mdiVote, mdiOpenInNew } from "@mdi/js";
import Icon from "@mdi/react"; 
import Transfer from "./Transfer";

class Wallet extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: undefined,
      loading: true,
    };
  }

  componentDidMount() {
    document.title = "Records";
    document.body.classList.add("white");
    var self = this;

    var url = API_URL + "user/getUserDetails";

    var payload = {
      token: localStorage.getItem("token"),
    
    };

    axios
      .post(url, payload)
      .then((result) => {
        console.log(result);
        self.setState({ data: result.data.data, loading: false });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  componentWillUnmount() {
    document.body.classList.remove("white");
  }

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {
    return (
      <React.Fragment>
      {this.state.data ?( <React.Fragment>
       
<div className="row">
    <div className="col-md-12">
        <div className="panel panel-default">
            <div className="panel-body row">
            <div className="col-md-2 text-right"><strong>Name :</strong></div>
            <div className="col-md-10">{this.state.data.name}</div>
            <div className="col-md-2 text-right"><strong>Account Balance:</strong></div>
            <div className="col-md-10">{this.state.data.walletAmount}</div>
           
            <div className="offset-md-2 col-md-10"><Transfer walletAmount={this.state.data.walletAmount} /></div>
                
            </div>
        </div>
      </div>
    </div>
         </React.Fragment>
    ):( <p>Unable to retrive data, please try again.</p>)}
    </React.Fragment>
    );
  }
}

export default Wallet;

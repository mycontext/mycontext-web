import React, { Component } from "react";
import axios from "axios";
import SubRecords from "../Team/SubRecords";
import {
  ToastsContainer,
  ToastsStore,
  ToastsContainerPosition,
} from "react-toasts";

import { API_URL } from "../../common";

class MyRecords extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: undefined,
      loading: true,
    };
  }

  componentDidMount() {
    document.title = "Medical Record";
    document.body.classList.add("white");
    var self = this;

    var url = API_URL + "record/getTeamRecord";

    var payload = {
      token: localStorage.getItem("token"),
      id: this.props.recordId,
    };

    axios
      .post(url, payload)
      .then(function (response) {
        if (response.data.success) {
          self.setState({ data: response.data.data, loading: false });
          console.log(self.state.data);
        } else {
          ToastsStore.error(response.data.message);
        }
      })
      .catch(function (error) {
        ToastsStore.error(error.response.data.message);
      });
  }

  componentWillUnmount() {
    document.body.classList.remove("white");
  }

  render() {
    return (
      <React.Fragment>
        <ToastsContainer
          store={ToastsStore}
          position={ToastsContainerPosition.TOP_RIGHT}
        />

        {this.state.data ? (
          <React.Fragment>
            <nav className="nav nav-pills nav-justified">
              <a
                className="nav-item nav-link active"
                href="#nav-medical-record"
                data-toggle="tab"
                role="tab"
                aria-controls="nav-medical-record"
                aria-selected="true"
              >
                My Profile
              </a>

              <a
                className="nav-item nav-link"
                href="#nav-medical-history"
                data-toggle="tab"
                role="tab"
                aria-controls="nav-medical-history"
                aria-selected="true"
              >
                My History
              </a>
            </nav>

            <div className="tab-content mt-4" id="nav-tabContent">
              <div
                className="tab-pane fade show active"
                id="nav-medical-record"
                role="tabpanel"
                aria-labelledby="nav-medical-record"
              >
                <h3>
                  <strong>Medical Treatment Team:</strong>{" "}
                  <a href="/my-team">{this.state.data.team.name}</a>
                </h3>
                <table className="table table-bordered ">
                  <tbody>
                    <tr>
                      <th width="30%">Blood Group</th>
                      <td>{this.state.data.blood_group}</td>
                    </tr>
                    <tr>
                      <th>Blood Pressure</th>
                      <td>{this.state.data.bpressure}</td>
                    </tr>
                    <tr>
                      <th>Cancer Type</th>
                      <td>{this.state.data.cancer_type}</td>
                    </tr>
                    <tr>
                      <th>Intial Tumor Size</th>
                      <td>{this.state.data.cs_tumor_size}</td>
                    </tr>
                    <tr>
                      <th>Weight</th>
                      <td>{this.state.data.cweight}</td>
                    </tr>{" "}
                    <tr>
                      <th>Date of Diagnosis</th>
                      <td>{this.state.data.date_of_diagnosis}</td>
                    </tr>
                    <tr>
                      <th>Gender</th>
                      <td>{this.state.data.gender}</td>
                    </tr>
                    <tr>
                      <th>Outcome Date</th>
                      <td>{this.state.data.outcome_date}</td>
                    </tr>
                    <tr>
                      <th>Year Of Birth</th>
                      <td>{this.state.data.year_of_birth}</td>
                    </tr>
                    <tr>
                      <th>Created On</th>
                      <td>{this.state.data.createdAt}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div
                className="tab-pane fade"
                id="nav-medical-history"
                role="tabpanel"
                aria-labelledby="nav-medical-history"
              >
                <SubRecords
                  teamId={this.props.teamId}
                  patientId={
                    this.state.data ? this.state.data.patient : undefined
                  }
                />
              </div>
            </div>
          </React.Fragment>
        ) : (
          <p>No medical records found, create one.</p>
        )}
      </React.Fragment>
    );
  }
}

export default MyRecords;

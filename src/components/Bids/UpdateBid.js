import React, { Component } from "react";
import axios from "axios";
import {
  ToastsContainer,
  ToastsStore,
  ToastsContainerPosition,
} from "react-toasts";
import { API_URL } from "../../common";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";


import Moment from "moment";
class UpdateBid extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      bidStartDate: new Date(),
      bidExpiryDate: new Date(),
      recordAccessStartDate: new Date(),
      recordAccessExpiryDate: new Date(),
      cancerTypes: [],
      bidId: "",
    };
  }

  componentDidMount() {
    console.log("Prps here", this.props);
    if (this.props) {
      var data = {
        recordsRequired: this.props.recordsRequired,
        cancerType: this.props.cancerType,
        amountPerRecord: this.props.amountPerRecord,
      };
      this.setState({ data: data });
      if (this.props.recordAccessStartDate) {
        this.setState({
          recordAccessStartDate: new Date(this.props.recordAccessStartDate),
        });
      }
      if (this.props.bidStartDate) {
        this.setState({
          bidStartDate: new Date(this.props.bidStartDate),
        });
      }
      if (this.props.recordAccessExpiryDate) {
        this.setState({
          recordAccessExpiryDate: new Date(this.props.recordAccessExpiryDate),
        });
      }
      if (this.props.bidExpiryDate) {
        this.setState({
          bidExpiryDate: new Date(this.props.bidExpiryDate),
        });
      }
      if (this.props.bidId) {
        this.setState({
          bidId:this.props.bidId,
        });
      }
    }
    this.getCancerList();
  }

  handleChange = (event) => {
    const dataCopy = this.state.data;
    dataCopy[event.target.name] = event.target.value;
    this.setState({ data: dataCopy });
  };
  bidStartDateChange = (date) => {
    this.setState({
      bidStartDate: date,
    });
  };

  bidExpiryDateChange = (date) => {
    this.setState({
      bidExpiryDate: date,
    });
  };
  rec;
  recordAccessStartDateChange = (date) => {
    this.setState({
      recordAccessStartDate: date,
    });
  };
  recordAccessExpiryDateChange = (date) => {
    this.setState({
      recordAccessExpiryDate: date,
    });
  };
  handleSubmit = (event) => {
    event.preventDefault();
    if (this.state.bidStartDate >= this.state.bidExpiryDate) {
      alert("Invalid Expiry Date");
      return;
    }
    if (this.state.bidExpiryDate >= this.state.recordAccessStartDate) {
      alert("Invalid Access Start Date");
      return;
    }
    if (this.state.recordAccessStartDate >= this.state.recordAccessExpiryDate) {
      alert("Invalid Access End Date");
      return;
    }

    var self = this;
    self.setState({ loading: true });

    var payload = {
      token: localStorage.getItem("token"),
      ...self.state.data,
      bidStartDate: Moment(this.state.bidStartDate).format("YYYY-MM-DD"),
      bidExpiryDate: Moment(this.state.bidExpiryDate).format("YYYY-MM-DD"),
      recordAccessStartDate: Moment(this.state.recordAccessStartDate).format(
        "YYYY-MM-DD"
      ),
      recordAccessExpiryDate: Moment(this.state.recordAccessExpiryDate).format(
        "YYYY-MM-DD"
      ),
      bidId: this.state.bidId,
      // bidId:this.props.bidId
    };

    var url = API_URL + "bid/editBid ";

    axios
      .post(url, payload)
      .then(function (response) {
        if (response.data.success) {
          window.location.reload();
          ToastsStore.success(response.data.message);
        } else {
          self.setState({ loading: false });
          ToastsStore.error(response.data.message);
        }
      })
      .catch(function (error) {
        self.setState({ loading: false });
        ToastsStore.error(error.response.data.message);
        console.log(error);
      });
  };
  // handleOptionChange = event => {
  //   var type = JSON.parse(event.target.value);
  //   let eve = { ...event }
  //   // this.setState(prevState => ({
  //   //   data: {
  //   //     ...prevState.data,
  //   //     name: user.name
  //   //   }
  //   // }));
  //   // this.state.data["name"] = user.name;
  //   this.setState({
  //     [eve.target.name]: event.target.value,
  //     // data: {
  //     //   ...prevState.data,
  //     //   name: user.name
  //     // }
  //   });
  // };
  getCancerList() {
    var _url = API_URL + "record/getAllCancerTypes";
    var self = this;
    self.setState({ loading: true });
    var _payload = { token: localStorage.getItem("token") };
    axios
      .post(_url, _payload)
      .then(function (response) {
        if (response.data.success) {
          console.log(response.data.data.cancerTypes);
          self.setState({
            loading: false,
            cancerTypes: response.data.data.cancerTypes,
          });
        } else {
          self.setState({ loading: false });
          ToastsStore.error(response.data.message);
        }
      })
      .catch(function (error) {
        self.setState({ loading: false });
        ToastsStore.error(error.response.data.message);
        console.log(error);
      });
  }

  render() {
    return (
      <React.Fragment>
        <ToastsContainer
          store={ToastsStore}
          position={ToastsContainerPosition.TOP_RIGHT}
        />
        <script src="/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
        <div className="text-right">
          <button
            type="button"
            className="btn btn-info mb-4 btn-block"
            data-toggle="modal"
            data-target={"#" + this.state.bidId}
          >
            Update bid
          </button>
        </div>
        <div
          className="modal fade"
          id={this.state.bidId}
          tabIndex="-1"
          role="dialog"
          aria-labelledby="createModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog  modal-lg" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="createModalLabel">
                  Update bid
                </h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                <form className="form-signin" onSubmit={this.handleSubmit}>
                  <div className="container-add row">
                    <div className="form-group col-md-12">
                      <label htmlFor="cancerType">Cancer type</label>
                      <select
                        className="form-control select"
                        name="cancerType"
                        value={this.state.data.cancerType}
                        onChange={this.handleChange}
                      >
                        <option value="" defaultValue>
                          Select Type
                        </option>
                        {this.state.cancerTypes.map((type, key) => {
                          return (
                            <option key={key} value={type}>
                              {type}
                            </option>
                          );
                        })}
                      </select>
                    </div>{" "}
                    <div className="form-group col-md-12">
                      <label htmlFor="recordsRequired">
                        Total Record Count:
                      </label>
                      <input
                        type="number"
                        min="0"
                        id="recordsRequired"
                        className="form-control"
                        placeholder="Count"
                        name="recordsRequired"
                        value={this.state.data.recordsRequired}
                        onChange={this.handleChange}
                        required
                      />
                    </div>
                    <div className="form-group col-md-12">
                      <label htmlFor="amountPerRecord">
                        Price Per Record $:
                      </label>
                      <input
                        type="number"
                        min="0"
                        id="amountPerRecord"
                        className="form-control"
                        placeholder="Price"
                        name="amountPerRecord"
                        value={this.state.data.amountPerRecord}
                        onChange={this.handleChange}
                        required
                      />
                    </div>
                    <div className="form-group col-md-6">
                      <label htmlFor="recordsRequired">Bid Publish Date:</label>
                      <div className="input-group ">
                        <DatePicker
                          selected={this.state.bidStartDate}
                          dateFormat="yyyy-MM-dd"
                          id="bidStartDate"
                          className="form-control "
                          onChange={this.bidStartDateChange}
                          minDate={new Date()}
                        />
                      </div>
                    </div>
                    <div className="form-group col-md-6">
                      <label htmlFor="recordsRequired">Bid Closing Date:</label>
                      <div className="input-group ">
                        <DatePicker
                          name="bidExpiryDate"
                          dateFormat="yyyy-MM-dd"
                          className="form-control"
                          minDate={this.state.bidStartDate}
                          selected={this.state.bidExpiryDate}
                          onChange={this.bidExpiryDateChange}
                        />
                      </div>
                    </div>
                    <div className="form-group col-md-6">
                      <label htmlFor="recordsRequired">
                        Access Start Date:
                      </label>
                      <div className="input-group ">
                        <DatePicker
                          name="recordAccessStartDate"
                          className="form-control"
                          dateFormat="yyyy-MM-dd"
                          minDate={this.state.bidExpiryDate}
                          selected={this.state.recordAccessStartDate}
                          onChange={this.recordAccessStartDateChange}
                        />
                      </div>
                    </div>
                    <div className="form-group col-md-6">
                      <label htmlFor="recordsRequired">Access End Date:</label>
                      <div className="input-group ">
                        {" "}
                        <DatePicker
                          name="recordAccessExpiryDate"
                          className="form-control"
                          dateFormat="yyyy-MM-dd"
                          minDate={this.state.recordAccessStartDate}
                          selected={this.state.recordAccessExpiryDate}
                          onChange={this.recordAccessExpiryDateChange}
                        />
                      </div>
                    </div>
                  </div>
                  <div className="modal-footer">
                    <button
                      type="button"
                      className="btn btn-secondary"
                      data-dismiss="modal"
                    >
                      Close
                    </button>
                    <button type="submit" className="btn btn-primary">
                      Save
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default UpdateBid;

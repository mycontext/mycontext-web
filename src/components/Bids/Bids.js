import React, { Component } from "react";
import {
  ToastsContainer,
  ToastsStore,
  ToastsContainerPosition,
} from "react-toasts";
import ActiveBids from "./ActiveBids";
import PendingBids from "./PendingBids";
import AcceptedBids from "./AcceptedBids";

class Bids extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: undefined,
    };
  }

  componentDidMount() {
    document.title = "Bids";
    document.body.classList.add("white");
  }

  componentWillUnmount() {
    document.body.classList.remove("white");
  }

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {
    return (
      <React.Fragment>
        <ToastsContainer
          store={ToastsStore}
          position={ToastsContainerPosition.TOP_RIGHT}
        />

        <nav className="nav nav-pills nav-justified">
          <a
            className="nav-item nav-link active"
            href="#nav-active-bids"
            data-toggle="tab"
            role="tab"
            aria-controls="nav-active-bids"
            aria-selected="true"
          >
            Active Bids
          </a>
          <a
            className="nav-item nav-link"
            href="#nav-pending-bids"
            data-toggle="tab"
            role="tab"
            aria-controls="nav-pending-bids"
            aria-selected="true"
          >
            Pending Bids
          </a>
          <a
            className="nav-item nav-link"
            href="#nav-accepted-bids"
            data-toggle="tab"
            role="tab"
            aria-controls="nav-accepted-bids"
            aria-selected="true"
          >
            Accepted Bids
          </a>
        </nav>

        <div className="tab-content mt-4" id="nav-tabContent">
          <div
            className="tab-pane fade show active"
            id="nav-active-bids"
            role="tabpanel"
            aria-labelledby="nav-active-bids"
          >
            <ActiveBids />
          </div>
          <div
            className="tab-pane fade"
            id="nav-pending-bids"
            role="tabpanel"
            aria-labelledby="nav-pending-bids"
          >
            <PendingBids />
          </div>
          <div
            className="tab-pane fade"
            id="nav-accepted-bids"
            role="tabpanel"
            aria-labelledby="nav-accepted-bids"
          >
            <AcceptedBids />
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Bids;

import React, { Component } from "react";
import axios from "axios";
import { ToastsStore } from "react-toasts";
import { API_URL } from "../../common";
import { mdiVote } from "@mdi/js";
import Icon from "@mdi/react";
import Moment from "moment";

class ActiveBids extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loading: true,
    };
  }

  componentDidMount() {
    document.title = "Records";
    document.body.classList.add("white");
    var self = this;

    var url = API_URL + "bid/getSellersBids";

    var payload = {
      token: localStorage.getItem("token"),
      from: 0,
      size: 100,
      status: 1,
    };

    axios
      .post(url, payload)
      .then((result) => {
        console.log(result.data);
        self.setState({ data: result.data.data, loading: false });
      })
      .catch((error) => {
        ToastsStore.error(error.response.data.message);
        console.log(error);
      });
  }

  componentWillUnmount() {
    document.body.classList.remove("white");
  }

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  acceptBid = (bid) => {
    var self = this;

    var url = API_URL + "bid/acceptBid";

    var payload = {
      token: localStorage.getItem("token"),
      bidId: bid.bidId,
      recordId: bid.recordId,
      buyerId: bid.bidBuyer._id,
      teamId: bid.teamId,
    };

    axios
      .post(url, payload)
      .then(function (response) {
        if (response.data.success) {
          window.location.reload();
          ToastsStore.success(response.data.message);
        } else {
          self.setState({ loading: false });
          ToastsStore.error(response.data.message);
        }
      })
      .catch(function (error) {
        self.setState({ loading: false });
        ToastsStore.error(error.response.data.message);
        console.log(error);
      });
  };

  render() {
    return (
      <React.Fragment>
        <div className="table-responsive">
          <table className="table align-items-center table-flush mt-2">
            <thead className="thead-light">
              <tr>
                <th scope="col">Buyer</th>
                <th scope="col">Team Name</th>
                <th scope="col">Expiry</th>
                <th scope="col">Bid Amount</th>
                <th scope="col">Your Share</th>
                <th scope="col">Accept</th>
              </tr>
            </thead>
            <tbody>
              {this.state.data.map((bid, index) => (
                <tr key={index}>
                  <td>
                    <div className="d-flex align-items-center">
                      <span> {bid.bidBuyer.name}</span>
                    </div>
                  </td>
                  <td>
                    <div className="d-flex align-items-center">
                      <span> {bid.teamName}</span>
                    </div>
                  </td>
                  <td>{Moment(bid.bidExpiry).format("LL")}</td>
                  <td>
                    <div className="d-flex align-items-center">
                      <span> {bid.bidAmountPerRecord}</span>
                    </div>
                  </td>
                  <td>
                    <div className="d-flex align-items-center">
                      <span className="badge badge-info mr-4">
                        {bid.bidPersonalEarning}
                      </span>
                    </div>
                  </td>

                  <td className="td-actions">
                    <button
                      type="button"
                      rel="tooltip"
                      className="btn btn-danger btn-icon btn-sm btn-simple"
                      data-original-title=""
                      title=""
                      onClick={() => this.acceptBid(bid)}
                    >
                      <Icon
                        path={mdiVote}
                        title="Accept Bid"
                        size={0.5}
                        horizontal
                        vertical
                        rotate={180}
                        color="#fff"
                      />
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </React.Fragment>
    );
  }
}

export default ActiveBids;

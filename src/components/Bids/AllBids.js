import React, { Component } from "react";
import axios from "axios";
// import { ToastsStore } from "react-toasts";
// import $ from "jquery";
import { API_URL } from "../../common";
import { mdiVote, mdiOpenInNew, mdiTableEdit } from "@mdi/js";
import Icon from "@mdi/react";
import Moment from "moment";
import UpdateBid from "./UpdateBid";

class AllBids extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loading: true,
    };
  }

  componentDidMount() {
    document.title = "all-bids";
    document.body.classList.add("white");
    var self = this;

    var url = API_URL + "bid/getBuyersBids";

    var payload = {
      token: localStorage.getItem("token"),
    };

    axios
      .post(url, payload)
      .then((result) => {
        self.setState({ data: result.data.data, loading: false });
        console.log(self.state.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  componentWillUnmount() {
    document.body.classList.remove("white");
  }

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {
    return (
      <React.Fragment>
        <div className="table-responsive">
          <table className="table align-items-center table-flush mt-2">
            <thead className="thead-light">
              <tr>
                <th scope="col">Cancer Type</th>

                <th scope="col">Bid Start Date</th>
                <th scope="col">Bid Expiry Date</th>
                <th scope="col">Amount (per record)</th>
                <th scope="col">Status</th>

                <th scope="col">Access Start Date</th>
                <th scope="col">Access Expiry Date</th>
                <th scope="col">View Records Bought</th>
                <th scope="col">Update</th>

                <th scope="col" />
              </tr>
            </thead>
            <tbody>
              {this.state.data.map((obj2, index) => (
                <tr key={index}>
                  <td>
                    <div className="d-flex align-items-center">
                      <span> {obj2.cancerType}</span>
                    </div>
                  </td>
                  <td>
                    <div className="d-flex align-items-center">
                      <span> {Moment(obj2.bidStartDate).format("LL")}</span>
                    </div>
                  </td>
                  <td>
                    <div className="d-flex align-items-center">
                      <span> {Moment(obj2.bidExpiryDate).format("LL")}</span>
                    </div>
                  </td>
                  <td>
                    <div className="d-flex align-items-center">
                      <span> {obj2.amountPerRecord}</span>
                    </div>
                  </td>
                  <td>
                    <div className="d-flex align-items-center">
                      <span> {obj2.bidStatus}</span>
                    </div>
                  </td>

                  <td>
                    <div className="d-flex align-items-center">
                      <span>
                        {" "}
                        {Moment(obj2.recordAccessStartDate).format("LL")}
                      </span>
                    </div>
                  </td>
                  <td>
                    <div className="d-flex align-items-center">
                      <span>
                        {" "}
                        {Moment(obj2.recordAccessExpiryDate).format("LL")}
                      </span>
                    </div>
                  </td>

                  <td className="td-actions text-right">
                    <a
                      type="button"
                      rel="tooltip"
                      className="btn btn-info btn-icon btn-sm btn-simple"
                      href={"/records-for-bid/" + obj2._id}
                    >
                      <Icon
                        path={mdiOpenInNew}
                        title="User Profile"
                        size={0.5}
                        horizontal
                        vertical
                        rotate={180}
                        color="#fff"
                      />
                    </a>
                  </td>
                  <td>
                    <UpdateBid
                      bidId={obj2._id}
                      cancerType={obj2 ? obj2.cancerType : undefined}
                      bidStartDate={obj2.bidStartDate}
                      bidExpiryDate={obj2.bidExpiryDate}
                      recordsRequired={obj2.recordsRequired}
                      recordAccessStartDate={obj2.recordAccessStartDate}
                      recordAccessExpiryDate={obj2.recordAccessExpiryDate}
                      amountPerRecord={obj2.amountPerRecord}
                    />
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </React.Fragment>
    );
  }
}

export default AllBids;

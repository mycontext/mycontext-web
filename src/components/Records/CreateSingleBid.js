import React, { Component } from "react";
import axios from "axios";
import {
  ToastsContainer,
  ToastsStore,
  ToastsContainerPosition,
} from "react-toasts";
import { API_URL } from "../../common";
import DatePicker from "react-datepicker"; 
import "react-datepicker/dist/react-datepicker.css";
import { mdiVote, mdiCartArrowDown } from "@mdi/js";
import Moment from "moment";
import Icon from "@mdi/react";
class CreateSingleBid extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      data: { },
       bidStartDate: new Date(),
       bidExpiryDate: new Date(),
       recordAccessStartDate: new Date(),
       recordAccessExpiryDate: new Date(),
       cancerTypes:[]  ,
       user_type: localStorage.getItem("user_type"), 
     
    };
  }
  componentDidMount() { 
  }

  handleChange = (event) => {
    const dataCopy = this.state.data;
    dataCopy[event.target.name] = event.target.value;
    this.setState({ data: dataCopy });
  };
  bidStartDateChange = date => {
    this.setState({
     bidStartDate:date
    });
  };
  
  bidExpiryDateChange = date => {
    this.setState({
      bidExpiryDate:date
    });
  };
  rec
  recordAccessStartDateChange = date => {
    this.setState({
      recordAccessStartDate:date
    });
  };
  recordAccessExpiryDateChange = date => {
    this.setState({
      recordAccessExpiryDate:date
    });
  };
  handleSubmit = (event) => 
  {
    event.preventDefault();
if(this.state.bidStartDate>=this.state.bidExpiryDate){
  alert("Invalid Expiry Date");
  return;
}
if(this.state.bidExpiryDate>=this.state.recordAccessStartDate){
  alert("Invalid Access Start Date");
  return;
}
if(this.state.recordAccessStartDate>=this.state.recordAccessExpiryDate)
{
  alert("Invalid Access End Date");
  return;
}

    var self = this;
    self.setState({ loading: true });

    var payload = {
      token: localStorage.getItem("token"),
       ...self.state.data ,
        recordsRequired:1,
        
        recordId:this.props.recordId,
        bidStartDate: Moment(this.state.bidStartDate).format("YYYY-MM-DD"),
        bidExpiryDate: Moment(this.state.bidExpiryDate).format("YYYY-MM-DD"),
        recordAccessStartDate: Moment(this.state.recordAccessStartDate).format("YYYY-MM-DD"),
        recordAccessExpiryDate: Moment(this.state.recordAccessExpiryDate).format("YYYY-MM-DD"),
        bidStatus: "ACTIVE"       
    };

    var url = API_URL + "bid/createSingleBid ";

    axios
      .post(url, payload)
      .then(function (response) {
        if (response.data.success) {
          window.location.reload();
          ToastsStore.success(response.data.message);
        } else {
          self.setState({ loading: false });
          ToastsStore.error(response.data.message);
        }
      })
      .catch(function (error) {
        self.setState({ loading: false });
        ToastsStore.error(error.response.data.message);
        console.log(error);
      });
  };
  // handleOptionChange = event => {
  //   var type = JSON.parse(event.target.value);
  //   let eve = { ...event }
  //   // this.setState(prevState => ({
  //   //   data: {
  //   //     ...prevState.data,
  //   //     name: user.name
  //   //   }
  //   // }));
  //   // this.state.data["name"] = user.name;
  //   this.setState({
  //     [eve.target.name]: event.target.value,
  //     // data: {
  //     //   ...prevState.data,
  //     //   name: user.name
  //     // }
  //   });
  // };
 

  render() {
    return (
      <React.Fragment>
        <ToastsContainer
          store={ToastsStore}
          position={ToastsContainerPosition.TOP_RIGHT}
        /> 
     
       
          <button
            type="button"
            
            className={
              (this.state.user_type === "Pharmaceutical Company" || this.state.user_type === "Pathology Laboratory"||this.state.user_type === "Insurance Company" ) ?  "btn btn-success btn-icon btn-sm btn-simple":"d-none"
            }
            data-toggle="modal"
            data-target="#createModal"
          >
              <Icon
                        path={mdiCartArrowDown}
                        title="create bid"
                        size={0.5}
                        horizontal
                        vertical
                        rotate={180}
                        color="#fff"
                      />
                    
          </button>
       {/* <span> {this.state.user_type} </span> */}
        <div
          className="modal fade"
          id="createModal"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="createModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog  modal-lg" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="createModalLabel">
                  Create Bid for this record
                </h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                <form className="form-signin text-left" onSubmit={this.handleSubmit}>
                  <div className="container-add row">
                  
                        <div className="form-group col-md-12">
                          <label htmlFor="cancerType">Patient Name: </label>
                         <strong>{this.props.patientName}</strong> 
                      
                        </div> 
                         <div className="form-group col-md-12">
                          <label htmlFor="cancerType">Cancer Type: </label>
                         <strong>{this.props.cancerType}</strong> 
                      
                        </div> 
                         
                        <div className="form-group col-md-12">
                          <label htmlFor="amountPerRecord">Price for Record $:</label>
                          <input 
                            type='number'
                            min="0"
                            id="amountPerRecord"
                            className="form-control"
                            placeholder="Price"
                            name="amountPerRecord"
                            value={this.state.data.amountPerRecord}
                            onChange={this.handleChange}
                            required
                          />
                        </div>
                        <div className="form-group col-md-6">
                          <label htmlFor="recordsRequired">Bid Publish Date:</label> 
                          <div className="input-group ">
                          <DatePicker
                              selected={this.state.bidStartDate}
                              dateFormat='yyyy-MM-dd'
                              id="bidStartDate"
                              className="form-control "
                              onChange={this.bidStartDateChange}
                              minDate={new Date()}
                            />
                            </div>                          
                         
                        </div>
                        <div className="form-group col-md-6">
                          <label htmlFor="recordsRequired">Bid Closing Date:</label>                           
                          <div className="input-group ">
                          <DatePicker
                               name="bidExpiryDate"
                               dateFormat='yyyy-MM-dd'
                                className="form-control"
                                minDate={this.state.bidStartDate}
                              selected={this.state.bidExpiryDate}
                              onChange={this.bidExpiryDateChange}
                            />
                        </div>
                        </div>
                        <div className="form-group col-md-6">
                          <label htmlFor="recordsRequired">Access Start Date:</label>                           
                          <div className="input-group "><DatePicker
                                name="recordAccessStartDate"
                                className="form-control"
                                dateFormat='yyyy-MM-dd'
                                minDate={this.state.bidExpiryDate}
                              selected={this.state.recordAccessStartDate}
                              onChange={this.recordAccessStartDateChange}
                            />
                        </div>
                        </div>
                        <div className="form-group col-md-6">
                          <label htmlFor="recordsRequired">Access End Date:</label>                           
                          <div className="input-group ">  <DatePicker
                              name="recordAccessExpiryDate"
                                className="form-control"
                                dateFormat='yyyy-MM-dd'
                                minDate={this.state.recordAccessStartDate}
                              selected={this.state.recordAccessExpiryDate}
                              onChange={this.recordAccessExpiryDateChange}
                            />
                               </div>
                        </div>
                        
                  </div>
                  <div className="modal-footer">
                    <button
                      type="button"
                      className="btn btn-secondary"
                      data-dismiss="modal"
                    >
                      Close
                    </button>
                    <button type="submit" className="btn btn-primary">
                      Save
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default CreateSingleBid;

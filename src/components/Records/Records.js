import React, { Component } from "react";
import axios from "axios";
// import { ToastsStore } from "react-toasts";
// import $ from "jquery";
import { API_URL } from "../../common";
import { mdiVote, mdiOpenInNew } from "@mdi/js";
import Icon from "@mdi/react";
import CreateSingleBid from "./CreateSingleBid";

class Records extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loading: true,
    };
  }

  componentDidMount() {
    document.title = "Records";
    document.body.classList.add("white");
    var self = this;

    var url = API_URL + "record/listRecords";

    var payload = {
      token: localStorage.getItem("token"),
      from: 0,
      size: 100,
    };

    axios
      .post(url, payload)
      .then((result) => {
        console.log(result);
        self.setState({ data: result.data.data, loading: false });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  componentWillUnmount() {
    document.body.classList.remove("white");
  }

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {
    return (
      <React.Fragment>
        <div className="table-responsive">
          <table className="table align-items-center table-flush mt-2">
            <thead className="thead-light">
              <tr>
                <th scope="col">Name</th>
                <th scope="col">gender</th>
                <th scope="col">Type</th>
                <th scope="col">Blood Group</th>
                <th scope="col">Year Of Birth</th>
                <th scope="col">Refered By</th>
                <th scope="col" />
              </tr>
            </thead>
            <tbody>
              {this.state.data.map((record, index) => (
                <tr key={index}>
                  <td>
                    <div className="d-flex align-items-center">
                      <span> {record.patient.name}</span>
                    </div>
                  </td>
                  <td>
                    <span className="badge badge-info mr-4">
                      {record.gender}
                    </span>
                  </td>
                  <td>
                    <div className="d-flex align-items-center">
                      <span> {record.cancer_type}</span>
                    </div>
                  </td>
                  <td>
                    <div className="d-flex align-items-center">
                      <span> {record.blood_group}</span>
                    </div>
                  </td>
                  <td>
                    <div className="d-flex align-items-center">
                      <span> {record.year_of_birth}</span>
                    </div>
                  </td>

                  <td>
                    <div className="d-flex align-items-center">
                      <span> {record.doctor.name}</span>
                    </div>
                  </td>
                  <td className="td-actions text-right">
                    <a
                      type="button"
                      rel="tooltip"
                      className="btn btn-info btn-icon btn-sm btn-simple"
                      href={"/view/" + record._id}
                    >
                      <Icon
                        path={mdiOpenInNew}
                        title="User Profile"
                        size={0.5}
                        horizontal
                        vertical
                        rotate={180}
                        color="#fff"
                      />
                    </a>
                    <CreateSingleBid  recordId={ record._id} cancerType={record.cancer_type} patientName={record.patient.name}/>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </React.Fragment>
    );
  }
}

export default Records;

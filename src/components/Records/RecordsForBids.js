import React, { Component } from "react";
import axios from "axios";
// import { ToastsStore } from "react-toasts";
// import $ from "jquery";
import { API_URL } from "../../common";
//import { mdiVodte, mdiOpenInNew } from "@mdi/js";
//import Icon from "@mdi/react";
import Moment from "moment";


class RecordsForBids extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loading: true,
    };
  }

  componentDidMount() {
    document.title = "Bid Records";
    document.body.classList.add("white");
    var self = this;

    var url = API_URL + "record/getRecordsForBid";

    var payload = {
      token: localStorage.getItem("token"),
      bidId: this.props.bidId
    };

    axios
      .post(url, payload)
      .then((result) => {
        self.setState({ data: result.data.data, loading: false });
        console.log(self.state.data);
      })
      .catch((error) => {
        console.log(error.response.data);
      });
  }

  componentWillUnmount() {
    document.body.classList.remove("white");
  }

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {
    return (
      <React.Fragment>
        <div className="table-responsive">
          <table className="table align-items-center table-flush mt-2">
            <thead className="thead-light">
              <tr>
                <th scope="col">age_at_diagnosis</th>
                <th scope="col">date_of_diagnosis</th>
                <th scope="col">cweight</th>
                <th scope="col">bpressure</th>
                <th scope="col">cs_tumor_size</th>
                <th scope="col">blood_group</th>
                <th scope="col">gender</th>
                <th scope="col">cancer_type</th>
                <th scope="col" />
              </tr>
            </thead>
            <tbody>
              {this.state.data.map((obj1, index) => (
                <tr key={index}>
                  <td>
                    <div className="d-flex align-items-center">
                      <span> {obj1.age_at_diagnosis}</span>
                    </div>
                  </td>
                  <td>
                    <div className="d-flex align-items-center">
                      <span> {Moment(obj1.date_of_diagnosis).format("LL")}</span>
                    </div>
                  </td>
                  <td>
                    <div className="d-flex align-items-center">
                      <span> {obj1.cweight}</span>
                    </div>
                  </td>

                  <td>
                    <div className="d-flex align-items-center">
                      <span> {obj1.bpressure}</span>
                    </div>
                  </td>
                  <td>
                    <div className="d-flex align-items-center">
                      <span> {obj1.cs_tumor_size}</span>
                    </div>
                  </td>
                  <td>
                    <div className="d-flex align-items-center">
                      <span> {obj1.blood_group}</span>
                    </div>
                  </td>
                  <td>
                    <span className="badge badge-info mr-4">
                      {obj1.gender}
                    </span>
                  </td>
                  <td>
                    <div className="d-flex align-items-center">
                      <span> {obj1.cancer_type}</span>
                    </div>
                  </td>

                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </React.Fragment>
    );
  }
}

export default RecordsForBids;

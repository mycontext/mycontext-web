import React, { Component } from "react";
import {
  ToastsContainer,
  ToastsStore,
  ToastsContainerPosition,
} from "react-toasts";
import Icon from "@mdi/react";
import { mdiAccount, mdiFileDocument, mdiGavel } from "@mdi/js";
import axios from "axios";
import { API_URL } from "../../common";

class Graphs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      reports: {
        patientsCount: 0,
        doctorCount: 0,
        medicalRecordsCount: 0,
        bidsCount: 0,
      },
    };
  }

  componentDidMount() {
    var self = this;

    var payload = {
      token: localStorage.getItem("token"),
    };

    var url = API_URL + "user/getCounts";

    axios
      .post(url, payload)
      .then(function (response) {
        if (response.data.success) {
          console.log(response.data.data);
          self.setState({
            patientsCount: response.data.patientsCount,
            doctorCount: response.data.doctorCount,
            medicalRecordsCount: response.data.medicalRecordsCount,
            bidsCount: response.data.bidsCount,
          });
        } else {
          ToastsStore.error(response.data.message);
        }
      })
      .catch(function (error) {
        ToastsStore.error(error.response.data.message);
      });
  }

  render() {
    return (
      <React.Fragment>
        <ToastsContainer
          store={ToastsStore}
          position={ToastsContainerPosition.TOP_RIGHT}
        />
        <div className="row">
          <div className="col-xl-3 col-lg-6">
            <div className="card card-stats mb-4 mb-xl-0">
              <div className="card-body">
                <div className="row">
                  <div className="col">
                    <h6 className="card-title text-uppercase text-muted mb-0">
                      Doctors
                    </h6>
                    <span className="h3 font-weight-bold mb-0">
                      {this.state.doctorCount}
                    </span>
                  </div>
                  <div className="col-auto">
                    <div className="icon icon-shape bg-danger text-white rounded-circle shadow">
                      <Icon
                        path={mdiAccount}
                        title="Dashboard"
                        size={1}
                        horizontal
                        vertical
                        rotate={180}
                        color="#ffffff"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-xl-3 col-lg-6">
            <div className="card card-stats mb-4 mb-xl-0">
              <div className="card-body">
                <div className="row">
                  <div className="col">
                    <h6 className="card-title text-uppercase text-muted mb-0">
                      Patients
                    </h6>
                    <span className="h3 font-weight-bold mb-0">
                      {this.state.patientsCount}
                    </span>
                  </div>
                  <div className="col-auto">
                    <div className="icon icon-shape bg-warning text-white rounded-circle shadow">
                      <Icon
                        path={mdiAccount}
                        title="Dashboard"
                        size={1}
                        horizontal
                        vertical
                        rotate={180}
                        color="#ffffff"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-xl-3 col-lg-6">
            <div className="card card-stats mb-4 mb-xl-0">
              <div className="card-body">
                <div className="row">
                  <div className="col">
                    <h6 className="card-title text-uppercase text-muted mb-0">
                      Records
                    </h6>
                    <span className="h3 font-weight-bold mb-0">
                      {this.state.medicalRecordsCount}
                    </span>
                  </div>
                  <div className="col-auto">
                    <div className="icon icon-shape bg-yellow text-white rounded-circle shadow">
                      <Icon
                        path={mdiFileDocument}
                        title="Dashboard"
                        size={1}
                        horizontal
                        vertical
                        rotate={180}
                        color="#ffffff"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-xl-3 col-lg-6">
            <div className="card card-stats mb-4 mb-xl-0">
              <div className="card-body">
                <div className="row">
                  <div className="col">
                    <h6 className="card-title text-uppercase text-muted mb-0">
                      Bids
                    </h6>
                    <span className="h3 font-weight-bold mb-0">
                      {this.state.bidsCount}
                    </span>
                  </div>
                  <div className="col-auto">
                    <div className="icon icon-shape bg-info text-white rounded-circle shadow">
                      <Icon
                        path={mdiGavel}
                        title="Dashboard"
                        size={1}
                        horizontal
                        vertical
                        rotate={180}
                        color="#ffffff"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Graphs;

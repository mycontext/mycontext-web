import React, { Component } from "react";
import axios from "axios";
import {
  ToastsContainer,
  ToastsStore,
  ToastsContainerPosition,
} from "react-toasts";
import Icon from "@mdi/react";
import { mdiOpenInNew } from "@mdi/js";
import Moment from "moment";
import { API_URL } from "../../common";

class Teams extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: undefined,
      loading: true,
    };
  }

  componentDidMount() {
    Moment.locale("en");
    document.title = "Team";
    document.body.classList.add("white");
    var self = this;

    var url = API_URL + "team/getDoctorTeams";

    var payload = {
      token: localStorage.getItem("token"),
    };

    axios
      .post(url, payload)
      .then(function (response) {
        if (response.data.success) {
          self.setState({ data: response.data.data, loading: false });
          console.log(self.state.data);
        } else {
          ToastsStore.error(response.data.message);
        }
      })
      .catch(function (error) {
      console.log(error)
      });
  }

  componentWillUnmount() {
    document.body.classList.remove("white");
  }

  render() {
    return (
      <React.Fragment>
        <ToastsContainer
          store={ToastsStore}
          position={ToastsContainerPosition.TOP_RIGHT}
        />

        {this.state.data ? (
          <React.Fragment>
            <div className="table-responsive">
              <table className="table align-items-center table-flush mt-2">
                <thead className="thead-light">
                  <tr>
                    <th scope="col">Patient</th>
                    <th scope="col">Email</th>
                    <th scope="col">Created On</th>
                    <th scope="col">Total Members</th>
                    <th scope="col" />
                  </tr>
                </thead>
                <tbody>
                  {this.state.data.map((team, index) => (
                    <tr key={index}>
                      <td>
                        <div className="d-flex align-items-center">
                          <span> {team.admin.name}</span>
                        </div>
                      </td>
                      <td>
                        <span>{team.admin.email}</span>
                      </td>
                      <td>
                        <div className="d-flex align-items-center">
                          <span>{Moment(team.createdAt).format("LL")}</span>
                        </div>
                      </td>
                      <td>
                        <div className="d-flex align-items-center text-center">
                          <span className="badge badge-success mr-4">
                            {team.members.length}
                          </span>
                        </div>
                      </td>
                      <td className="td-actions text-right">
                        <a
                          type="button"
                          rel="tooltip"
                          className="btn btn-info btn-icon btn-sm btn-simple"
                          href={"/team/" + team._id}
                        >
                          <Icon
                            path={mdiOpenInNew}
                            title="User Profile"
                            size={0.5}
                            horizontal
                            vertical
                            rotate={180}
                            color="#fff"
                          />
                        </a>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </React.Fragment>
        ) : (
          <p>No medical records found, create one.</p>
        )}
      </React.Fragment>
    );
  }
}

export default Teams;

import React, { Component } from "react";
import axios from "axios";
import { ToastsStore } from "react-toasts";
import { API_URL } from "../../common";
import Moment from "moment";
import UpdateBid from "../Bids/UpdateBid";
import { mdiDelete } from "@mdi/js";
import Icon from "@mdi/react";

class BuyerActiveBids extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loading: true,
    };
  }

  componentDidMount() {
    document.title = "Current Bids";
    document.body.classList.add("white");
    var self = this;

    var url = API_URL + "bid/getBuyersBids";

    var payload = {
      token: localStorage.getItem("token"),
      from: 0,
      size: 100,
    };

    axios
      .post(url, payload)
      .then((result) => {
        console.log(result.data);
        self.setState({ data: result.data.data, loading: false });
      })
      .catch((error) => {
        console.log(error);
        ToastsStore.error(error.response.data.message);
        console.log(error);
      });
  }
  deactivateBid = (bidId, e) => {
    var self = this;

    var url = API_URL + "bid/updateBidStatus";

    var payload = {
      token: localStorage.getItem("token"),
      bidId: bidId,
    };

    axios
      .post(url, payload)
      .then((result) => {
        console.log(result.data);
        if (result.data.success) {
          window.location.reload();
          ToastsStore.success(result.data.message);
        } else {
          ToastsStore.error(result.data.message);
        }
      })
      .catch((error) => {
        console.log(error);
        ToastsStore.error(error.response.data.message);
        console.log(error);
      });
  };

  componentWillUnmount() {
    document.body.classList.remove("white");
  }

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {
    return (
      <React.Fragment>
        <div className="table-responsive">
          <table className="table align-items-center table-flush mt-2">
            <thead className="thead-light">
              <tr>
                <th scope="col">Title</th>
                <th scope="col">Type</th>
                <th scope="col">Release/Closing Date</th>
                <th scope="col">Bid Amount </th>
                <th scope="col">Record Count</th>
                <th scope="col">Status</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
              {this.state.data.map((bid, index) => (
                <tr key={index}>
                  <td>
                    <div className="d-flex align-items-center">
                      <span> {bid.bidTitle}</span>
                    </div>
                  </td>
                  <td>
                    <div className="d-flex align-items-center">
                      <span> {bid.cancerType}</span>
                    </div>
                  </td>
                  <td>
                    {Moment(bid.bidStartDate).format("YYYY/MM/DD")} -{" "}
                    {Moment(bid.bidExpiryDate).format("YYYY/MM/DD")}
                  </td>
                  <td>
                    <div className="d-flex align-items-center">
                      <span> {bid.amountPerRecord}</span>
                    </div>
                  </td>
                  <td>
                    <div className="d-flex align-items-center">
                      <span className="badge badge-info mr-4">
                        {bid.recordsRequired}
                      </span>
                    </div>
                  </td>
                  <td>
                    <div className="d-flex align-items-center">
                      <span className="badge badge-info mr-4">
                        {bid.bidStatus}
                      </span>
                    </div>
                  </td>
                  {new Date(bid.bidExpiryDate) > new Date() ? (
                    <td>
                      <td className="td-actions text-right">
                        <a
                          type="button"
                          rel="tooltip"
                          className="btn btn-info btn-icon btn-sm btn-simple"
                          onClick={(e) => this.deactivateBid(bid._id, e)}
                        >
                          <Icon
                            path={mdiDelete}
                            title="Delete bid"
                            size={0.5}
                            horizontal
                            vertical
                            rotate={180}
                            color="#fff"
                          />
                        </a>
                      </td>
                    </td>
                  ) : null}
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </React.Fragment>
    );
  }
}

export default BuyerActiveBids;

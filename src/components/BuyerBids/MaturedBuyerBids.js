import React, { Component } from "react";
import axios from "axios";
import { ToastsStore } from "react-toasts";
import { API_URL } from "../../common";
import { mdiVote } from "@mdi/js";
import Icon from "@mdi/react";
import Moment from "moment";

class MaturedBuyerBids extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loading: true,
    };
  }

  componentDidMount() {
    document.title = "Matured Bids";
    document.body.classList.add("white");
    var self = this;

    var url = API_URL + "bid/getBuyersMaturedBids";

    var payload = {
      token: localStorage.getItem("token"),
      from: 0,
      size: 100,
      status: 1,
    };

    axios
      .post(url, payload)
      .then((result) => {
        console.log(result.data);
        self.setState({ data: result.data.data, loading: false });
      })
      .catch((error) => {
        ToastsStore.error(error.response.data.message);
        console.log(error);
      });
  }

  componentWillUnmount() {
    document.body.classList.remove("white");
  }

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {
    return (
      <React.Fragment>
        <div className="table-responsive">
          <table className="table align-items-center table-flush mt-2">
            <thead className="thead-light">
              <tr>
                <th scope="col">Title</th>
                <th scope="col">Validity Period</th>
                <th scope="col">Available/Total</th>
                <th scope="col">Total Budget Proposed</th>
                <th scope="col">Amount to be Paid</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
              {this.state.data.map((bid, index) => (
                <tr key={index}>
                  <td>
                    <div className="d-flex align-items-center">
                      <span> {bid.bidName}</span>
                    </div>
                  </td>

                  <td>
                    {Moment(bid.recordAccessStartDate).format("YYYY/MM/DD")} to
                    {Moment(bid.recordAccessExpiryDate).format("YYYY/MM/DD")}
                  </td>
                  <td>
                    <div className="d-flex align-items-center">
                      <span>
                        {" "}
                        {bid.numberOfRecordsAcquired}/
                        {bid.numberOfRecordsRequired}
                      </span>
                    </div>
                  </td>
                  <td>
                    <div className="d-flex align-items-center">
                      <span className="badge badge-info mr-4">
                        {" "}
                        ${bid.amountPerRecord * bid.numberOfRecordsRequired}
                      </span>
                    </div>
                  </td>
                  <td>
                    <div className="d-flex align-items-center">
                      <span className="badge badge-info mr-4">
                        {" "}
                        ${bid.amountPerRecord * bid.numberOfRecordsAcquired}
                      </span>
                    </div>
                  </td>
                  <td className="td-actions">
                    <button
                      type="button"
                      rel="tooltip"
                      className="btn btn-danger btn-icon btn-sm btn-simple"
                      data-original-title=""
                      title=""
                    >
                      <Icon
                        path={mdiVote}
                        title="User Profile"
                        size={0.5}
                        horizontal
                        vertical
                        rotate={180}
                        color="#fff"
                      />
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </React.Fragment>
    );
  }
}

export default MaturedBuyerBids;

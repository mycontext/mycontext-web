import React, { Component } from "react";
import {
  ToastsContainer,
  ToastsStore,
  ToastsContainerPosition,
} from "react-toasts"; 
import MaturedBuyerBids from "./MaturedBuyerBids";
import BuyerActiveBids from "./BuyerActiveBids";
import CreateBid from "./CreateBid";

class BuyerBids extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: undefined,
    };
  }

  componentDidMount() {
    document.title = "My Bids";
    document.body.classList.add("white");
  }

  componentWillUnmount() {
    document.body.classList.remove("white");
  }

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {
    return (
      <React.Fragment>
        <ToastsContainer
          store={ToastsStore}
          position={ToastsContainerPosition.TOP_RIGHT}
        />

        <nav className="nav nav-pills nav-justified">
          <a
            className="nav-item nav-link active"
            href="#nav-active-bids"
            data-toggle="tab"
            role="tab"
            aria-controls="nav-active-bids"
            aria-selected="true"
          >
           All Bids
          </a>
           
          <a
            className="nav-item nav-link"
            href="#nav-matured-bids"
            data-toggle="tab"
            role="tab"
            aria-controls="nav-matured-bids"
            aria-selected="true"
          >
            Matured Bids
          </a>
        </nav>

        <div className="tab-content mt-4" id="nav-tabContent">
          <div
            className="tab-pane fade show active"
            id="nav-active-bids"
            role="tabpanel"
            aria-labelledby="nav-active-bids"
          >
               <CreateBid />
            <BuyerActiveBids />
          </div>
          
          <div
            className="tab-pane fade"
            id="nav-matured-bids"
            role="tabpanel"
            aria-labelledby="nav-matured-bids"
          >
            <MaturedBuyerBids />
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default BuyerBids;

import React, { Component } from "react";
import { Line, Bar, Pie } from "react-chartjs-2";
import axios from "axios";
import {
  ToastsContainer,
  ToastsStore,
  ToastsContainerPosition,
} from "react-toasts";
import { API_URL } from "../../common";

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cancerByTypeData: {},
      ageGroupData: {},
      byGenderData: {},
      byBloodGroupData: {},
    };
  }

  componentDidMount() {
    const self = this;

    var payload = {
      token: localStorage.getItem("token"),
    };

    var url = API_URL + "user/getGraphsData";

    axios
      .post(url, payload)
      .then(function (response) {
        if (response.data.success) {
          console.log(response.data);
          const cancerByType = response.data.cancerByType;
          var labels = [];
          var counts = [];

          for (const value in cancerByType) {
            labels.push(cancerByType[value]._id);
            counts.push(cancerByType[value].count);
          }

          const cancerByTypeData = {
            labels: labels,
            datasets: [
              {
                label: "Data",
                fill: false,
                lineTension: 0.1,
                borderCapStyle: "butt",
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: "miter",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(75,192,192,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: counts,
                backgroundColor: [
                  "#1c7ed6",
                  "#0ca678",
                  "#f03e3e",
                  "#d6336c",
                  "#ae3ec9",
                  "#7048e8",
                  "#4263eb",
                  "#1098ad",
                  "#37b24d",
                  "#74b816",
                  "#f59f00",
                  "#f76707",
                ],
              },
            ],
          };

          const byBloodGroup = response.data.byBloodGroup;
          labels = [];
          counts = [];

          for (const value in byBloodGroup) {
            labels.push(byBloodGroup[value]._id);
            counts.push(byBloodGroup[value].count);
          }

          const byBloodGroupData = {
            labels: labels,
            datasets: [
              {
                label: "Data",
                fill: false,
                lineTension: 0.1,
                borderCapStyle: "butt",
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: "miter",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(75,192,192,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: counts,
                backgroundColor: [
                  "#f59f00",
                  "#f76707",
                  "#d6336c",
                  "#ae3ec9",
                  "#7048e8",
                  "#4263eb",
                  "#1098ad",
                  "#37b24d",
                  "#74b816",
                  "#1c7ed6",
                  "#0ca678",
                  "#f03e3e",
                ],
              },
            ],
          };

          const byGender = response.data.byGender;

          const byGenderDatasets = [];
          counts = [];

          for (const value in byGender) {
            byGenderDatasets.push({
              label: byGender[value]._id,
              fill: false,
              lineTension: 0.1,
              borderCapStyle: "butt",
              borderDash: [],
              borderDashOffset: 0.0,
              borderJoinStyle: "miter",
              pointBackgroundColor: "#fff",
              pointBorderWidth: 1,
              pointHoverRadius: 5,
              pointHoverBackgroundColor: "rgba(75,192,192,1)",
              pointHoverBorderColor: "rgba(220,220,220,1)",
              pointHoverBorderWidth: 2,
              pointRadius: 1,
              pointHitRadius: 10,
              data: [byGender[value].count],
              backgroundColor: [
                byGender[value]._id === "Male" ? "#0ca678" : "#f03e3e",
                byGender[value]._id === "Male" ? "#f03e3e" : "#0ca678",
              ],
            });
          }

          const byGenderData = {
            datasets: byGenderDatasets,
          };

          labels = [];
          counts = [];

          const ageGroup = response.data.ageGroup;

          for (const value in ageGroup) {
            labels.push(ageGroup[value]._id);
            counts.push(ageGroup[value].count);
          }

          const ageGroupData = {
            labels: labels,
            datasets: [
              {
                data: counts,
                backgroundColor: [
                  "#1c7ed6",
                  "#0ca678",
                  "#f03e3e",
                  "#d6336c",
                  "#ae3ec9",
                  "#7048e8",
                  "#4263eb",
                  "#1098ad",
                  "#37b24d",
                  "#74b816",
                  "#f59f00",
                  "#f76707",
                ],
              },
            ],
          };

          self.setState({
            cancerByTypeData: cancerByTypeData,
            ageGroupData: ageGroupData,
            byGenderData: byGenderData,
            byBloodGroupData: byBloodGroupData,
          });
        } else {
          ToastsStore.error(response.data.message);
        }
      })
      .catch(function (error) {
        ToastsStore.error(error.response.data.message);
      });
  }

  render() {
    return (
      <React.Fragment>
        <ToastsContainer
          store={ToastsStore}
          position={ToastsContainerPosition.TOP_RIGHT}
        />
        <div className="row">
          <div className="col-xl-7 mb-5 mb-xl-0">
            <div className="card bg-gradient-default shadow">
              <div className="card-header bg-transparent">
                <div className="row align-items-center">
                  <div className="col">
                    <h6 className="text-uppercase text-light ls-1 mb-1">
                      Cancer Type
                    </h6>
                  </div>
                </div>
              </div>
              <div className="card-body">
                <div className="chart">
                  <Pie
                    data={this.state.cancerByTypeData}
                    width={100}
                    height={50}
                    options={{
                      maintainAspectRatio: false,
                    }}
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="col-xl-5">
            <div className="card shadow">
              <div className="card-header bg-transparent">
                <div className="row align-items-center">
                  <div className="col">
                    <h6 className="text-uppercase text-muted ls-1 mb-1">
                      Gender
                    </h6>
                  </div>
                </div>
              </div>
              <div className="card-body">
                <div className="chart">
                  <Bar
                    data={this.state.byGenderData}
                    options={{
                      maintainAspectRatio: false,
                    }}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="row mt-4">
          <div className="col-xl-6">
            <div className="card shadow">
              <div className="card-header bg-transparent">
                <div className="row align-items-center">
                  <div className="col">
                    <h6 className="text-uppercase text-muted ls-1 mb-1">
                      Age Group
                    </h6>
                  </div>
                </div>
              </div>
              <div className="card-body">
                <div className="chart">
                  <Bar
                    data={this.state.ageGroupData}
                    options={{
                      maintainAspectRatio: false,
                    }}
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="col-xl-6 mb-5 mb-xl-0">
            <div className="card bg-gradient-default shadow">
              <div className="card-header bg-transparent">
                <div className="row align-items-center">
                  <div className="col">
                    <h6 className="text-uppercase text-light ls-1 mb-1">
                      Blood Group
                    </h6>
                  </div>
                </div>
              </div>
              <div className="card-body">
                <div className="chart">
                  <Pie
                    data={this.state.byBloodGroupData}
                    width={100}
                    height={50}
                    options={{
                      maintainAspectRatio: false,
                    }}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Dashboard;

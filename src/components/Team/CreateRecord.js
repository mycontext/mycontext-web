import React, { Component } from "react";
import axios from "axios";
import {
  ToastsContainer,
  ToastsStore,
  ToastsContainerPosition,
} from "react-toasts";
import { API_URL } from "../../common";

class CreateRecord extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
    };
  }

  componentDidMount() {
  }

  handleChange = (event) => {
    const dataCopy = this.state.data;
    dataCopy[event.target.name] = event.target.value;
    this.setState({ data: dataCopy });
  };
  handleSubmit = (event) => {
    event.preventDefault();

    let self = this;
    var url = API_URL + "record/addRecord";
    var payload = {
      token: localStorage.getItem("token"),
      data: {
        ...self.state.data,
      },
      patientId: self.props.patientId,
      teamId: self.props.teamId,
    };

    axios
      .post(url, payload)
      .then(function (response) {
        window.location.href = "/team/" + self.props.teamId;
      })
      .catch(function (error) {
        ToastsStore.error(error.response.data.message);
      });
  };

  render() {
    return (
      <React.Fragment>
        <ToastsContainer
          store={ToastsStore}
          position={ToastsContainerPosition.TOP_RIGHT}
        />
        <div className="text-right">
          <button
            type="button"
            className="btn btn-danger mb-4"
            data-toggle="modal"
            data-target="#createRecordModal"
          >
            Add
          </button>
        </div>
        <div
          className="modal fade"
          id="createRecordModal"
          tabindex="-1"
          role="dialog"
          aria-labelledby="createRecordModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog  modal-xl" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="createRecordModalLabel">
                  Insert Medical Record
                </h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <form className="form-signin" onSubmit={this.handleSubmit}>
                <div className="modal-body">
                  <div className="container-add">
                    <div className="row">
                      <div className="col-md-6">
                        <div className="form-label-group">
                          <label htmlFor="cancer_type">Cancer type</label>
                          <input
                            type="text"
                            id="cancer_type"
                            className="form-control"
                            placeholder="cancer type"
                            name="cancer_type"
                            value={this.state.data.cancer_type}
                            onChange={this.handleChange}
                            required
                          />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="form-label-group">
                          <label htmlFor="age_at_diagnosis">
                            Age at diagnosis
                          </label>
                          <input
                            type="text"
                            id="age_at_diagnosis"
                            className="form-control"
                            placeholder="age at diagnosis"
                            name="age_at_diagnosis"
                            value={this.state.data.age_at_diagnosis}
                            onChange={this.handleChange}
                            required
                          />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="form-label-group">
                          <label htmlFor="cs_tumor_size">Tumor size</label>
                          <input
                            type="text"
                            id="cs_tumor_size"
                            className="form-control"
                            placeholder="tumor size"
                            name="cs_tumor_size"
                            value={this.state.data.cs_tumor_size}
                            onChange={this.handleChange}
                            required
                          />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="form-label-group">
                          <label htmlFor="gender">Gender</label>
                          <input
                            type="text"
                            id="gender"
                            className="form-control"
                            placeholder="gender"
                            name="gender"
                            value={this.state.data.gender}
                            onChange={this.handleChange}
                            required
                          />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="form-label-group">
                          <label htmlFor="year_of_birth">Year of birth</label>
                          <input
                            type="text"
                            id="year_of_birth"
                            className="form-control"
                            placeholder="year of birth"
                            name="year_of_birth"
                            value={this.state.data.year_of_birth}
                            onChange={this.handleChange}
                            required
                          />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="form-label-group">
                          <label htmlFor="date_of_diagnosis">
                            Date of diagnosis
                          </label>
                          <input
                            type="text"
                            id="date_of_diagnosis"
                            className="form-control"
                            placeholder="date of diagnosis"
                            name="date_of_diagnosis"
                            value={this.state.data.date_of_diagnosis}
                            onChange={this.handleChange}
                            required
                          />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="form-label-group">
                          <label htmlFor="cweight">Weight</label>
                          <input
                            type="text"
                            id="cweight"
                            className="form-control"
                            placeholder="weight"
                            name="cweight"
                            value={this.state.data.cweight}
                            onChange={this.handleChange}
                            required
                          />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="form-label-group">
                          <label htmlFor="bpressure">Blood pressure</label>
                          <input
                            type="text"
                            id="bpressure"
                            className="form-control"
                            placeholder="blood pressure"
                            name="bpressure"
                            value={this.state.data.bpressure}
                            onChange={this.handleChange}
                            required
                          />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="form-label-group">
                          <label htmlFor="blood_group">Blood group</label>
                          <input
                            type="text"
                            id="blood_group"
                            className="form-control"
                            placeholder="blood group"
                            name="blood_group"
                            value={this.state.data.blood_group}
                            onChange={this.handleChange}
                            required
                          />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="form-label-group">
                          <label htmlFor="visit_date">Visit date</label>
                          <input
                            type="text"
                            id="visit_date"
                            className="form-control"
                            placeholder="visit date"
                            name="visit_date"
                            value={this.state.data.visit_date}
                            onChange={this.handleChange}
                            required
                          />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="form-label-group">
                          <label htmlFor="outcome_date">Outcome date</label>
                          <input
                            type="text"
                            id="outcome_date"
                            className="form-control"
                            placeholder="outcome date"
                            name="outcome_date"
                            value={this.state.data.outcome_date}
                            onChange={this.handleChange}
                            required
                          />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="form-label-group">
                          <label htmlFor="outcome">outcome</label>
                          <textarea
                            type="text"
                            id="outcome"
                            className="form-control"
                            placeholder="outcome"
                            name="outcome"
                            value={this.state.data.outcome}
                            onChange={this.handleChange}
                            required
                          ></textarea>
                        </div>
                      </div>
                    </div>
                    {this.state.subComponents}
                  </div>
                </div>
                <div className="modal-footer">
                  <button
                    type="button"
                    className="btn btn-sm btn-secondary"
                    data-dismiss="modal"
                  >
                    Close
                  </button>
                  <button type="submit" className="btn btn-sm  btn-primary">
                    Save
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default CreateRecord;

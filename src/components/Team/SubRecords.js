import React, { Component } from "react";
import axios from "axios";
import {
  ToastsContainer,
  ToastsStore,
  ToastsContainerPosition,
} from "react-toasts";
import Moment from "moment";
import { API_URL } from "../../common";

class SubRecords extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: undefined,
      loading: true,
    };
  }

  componentDidMount() {
    console.log(this.props.teamId)
    console.log(this.props.patientId)
    
    var self = this;
    Moment.locale("en");
    var url = API_URL + "subrecord/list";

    var payload = {
      token: localStorage.getItem("token"),
      teamId: this.props.teamId,
    };

    axios
      .post(url, payload)
      .then(function (response) {
        if (response.data.success) {
          self.setState({ data: response.data.data, loading: false });
          console.log(self.state.data);
        } else {
          ToastsStore.error(response.data.message);
        }
      })
      .catch(function (error) {
        ToastsStore.error(error.response.data.message);
      });
  }

  render() {
    return (
      <React.Fragment>
        <ToastsContainer
          store={ToastsStore}
          position={ToastsContainerPosition.TOP_RIGHT}
        />
       
        {this.state.data && this.state.data.length > 0 ? (
          this.state.data.map((subrecord, key) => (
            <div id={"accordion" + key} key={key}>
              <div className="card">
                <div className="card-header bg-primary" id="headingOne">
                  <h5 className="mb-0">
                    <button
                      className="btn btn-link collapsed text-white"
                      data-toggle="collapse"
                      data-target={"#collapseOne" + key}
                      aria-expanded="true"
                      aria-controls={"collapseOne" + key}
                    >
                      {Moment(subrecord.createdAt).format("LLL")}
                    </button>
                  </h5>
                </div>

                <div
                  id={"collapseOne" + key}
                  className="collapse"
                  aria-labelledby="headingOne"
                  data-parent={"#accordion" + key}
                >
                  <div className="card-body">
                    <SubRecordTable data={JSON.parse(subrecord.data)} />
                  </div>
                </div>
              </div>
            </div>
          ))
        ) : (
          <p>No medical history found</p>
        )}
      </React.Fragment>
    );
  }
}

const SubRecordTable = (props) => {
  const data = props.data;

  return (
    <table className="table table-striped table-bordered table-set">
      <tbody>
        {data.map((dict, key) => (
          <tr key={key}>
            <th>{dict.key}</th>
            <td>{dict.value}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default SubRecords;

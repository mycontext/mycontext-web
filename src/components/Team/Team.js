import React, { Component } from 'react';
import axios from 'axios';
import {
  ToastsContainer,
  ToastsStore,
  ToastsContainerPosition,
} from 'react-toasts';
import Record from '../ViewRecord/Record';
import Members from './Members';
import SubRecords from './SubRecords';
import CreateRecord from './CreateRecord';
import CreateSubRecord from './CreateSubRecord';
import Documents from './Documents';
import { API_URL } from '../../common';

class Team extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: undefined,
      team: undefined,
      loading: true,
    };
  }

  componentDidMount() {
    document.title = 'Team';
    document.body.classList.add('white');
    var self = this;

    var url = API_URL + 'record/getTeamRecord';

    var payload = {
      token: localStorage.getItem('token'),
      teamId: this.props.teamId,
    };

    axios
      .post(url, payload)
      .then(function (response) {
        if (response.data.success) {
          self.setState({
            data: response.data.data,
            team: response.data.team,
            loading: false,
          });
          console.log(response);
        } else {
          ToastsStore.error(response.data.message);
        }
      })
      .catch(function (error) {
        console.log(error);

        ToastsStore.error(error.response.data.message);
      });
  }

  componentWillUnmount() {
    document.body.classList.remove('white');
  }

  render() {
    return (
      <React.Fragment>
        <ToastsContainer
          store={ToastsStore}
          position={ToastsContainerPosition.TOP_RIGHT}
        />
        <h3>
          {this.state.data
            ? this.state.team.admin.name + ' Medical Record'
            : ''}
        </h3>

        <nav className="nav nav-pills nav-justified">
          <a
            className="nav-item nav-link active"
            href="#nav-medical-record"
            data-toggle="tab"
            role="tab"
            aria-controls="nav-medical-record"
            aria-selected="true">
            Medical Record
          </a>
          <a
            className="nav-item nav-link"
            href="#nav-team-members"
            data-toggle="tab"
            role="tab"
            aria-controls="nav-medical-members"
            aria-selected="true">
            Members
          </a>
          <a
            className="nav-item nav-link"
            href="#nav-medical-history"
            data-toggle="tab"
            role="tab"
            aria-controls="nav-medical-history"
            aria-selected="true">
            Medical History
          </a>
          <a
            className="nav-item nav-link"
            href="#nav-medical-documents"
            data-toggle="tab"
            role="tab"
            aria-controls="nav-medical-documents"
            aria-selected="true">
            Documents
          </a>
        </nav>

        <div className="tab-content mt-4" id="nav-tabContent">
          <div
            className="tab-pane fade show active"
            id="nav-medical-record"
            role="tabpanel"
            aria-labelledby="nav-medical-record">
            {this.state.data ? (
              ''
            ) : (
                <CreateRecord
                  teamId={this.props.teamId}
                  patientId={
                    this.state.team ? this.state.team.admin._id : undefined
                  }
                />
              )}
            <Record data={this.state.data} />
          </div>
          <div
            className="tab-pane fade"
            id="nav-team-members"
            role="tabpanel"
            aria-labelledby="nav-team-members">
            <Members
              admin={this.state.team ? this.state.team.admin : undefined}
              members={this.state.team ? this.state.team.members : undefined}
            />
          </div>
          <div
            className="tab-pane fade"
            id="nav-medical-history"
            role="tabpanel"
            aria-labelledby="nav-medical-history">
            <CreateSubRecord
              teamId={this.props.teamId}
              patientId={
                this.state.team ? this.state.team.admin._id : undefined
              }
            />
            <SubRecords
              teamId={this.props.teamId}
              patientId={
                this.state.team ? this.state.team.admin._id : undefined
              }
            />
          </div>
          <div
            className="tab-pane fade"
            id="nav-medical-documents"
            role="tabpanel"
            aria-labelledby="nav-medical-documents">
            <Documents
              teamId={this.props.teamId}
              patientId={
                this.state.team ? this.state.team.admin._id : undefined
              }
              recordId={this.state.data ? this.state.data._id : undefined}
              documents={this.state.data ? this.state.data.files : []}
            />
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Team;

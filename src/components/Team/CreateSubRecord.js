import React, { Component } from "react";
import axios from "axios";
import {
  ToastsContainer,
  ToastsStore,
  ToastsContainerPosition,
} from "react-toasts";
import $ from "jquery";
import { API_URL } from "../../common";

class CreateSubRecord extends Component {
  constructor(props) {
    super(props);
    this.state = {
      subComponents: [],
      ids: [0],
    };
  }

  componentDidMount() {
    const self = this;
    $(document).ready(function () {
      var wrapper = $(".container-add");
      var add_button = $("#button-new");

      var id = 1;

      $(add_button).click(function (e) {
        e.preventDefault();
        var className = id;
        self.setState({
          ids: [...self.state.ids, id],
        });

        self.setState({
          subComponents: [
            ...self.state.subComponents,
            <React.Fragment>
              <div className={className}>
                <div className="row">
                  <div className="col-5">
                    <div className="form-group">
                      <input
                        type="text"
                        className="form-control form-control-alternative"
                        name={"title" + id}
                        value={self.state["title" + id]}
                        onChange={self.handleChange}
                        placeholder="Title"
                        required
                      />
                    </div>
                  </div>
                  <div className="col-5">
                    <div className="form-group">
                      <input
                        type="text"
                        className="form-control form-control-alternative"
                        name={"value" + id}
                        value={self.state["value" + id]}
                        onChange={self.handleChange}
                        placeholder="Value"
                        required
                      />
                    </div>
                  </div>
                  <div className="col-2 text-center">
                    <button
                      type="button"
                      rel="tooltip"
                      className="btn btn-danger btn-icon btn-sm btn-simple delete"
                      data-original-title=""
                      title=""
                      style={{ height: "32px", marginTop: "8px" }}
                    >
                      <h6 className="text-white">X</h6>
                    </button>
                  </div>
                </div>
              </div>
            </React.Fragment>,
          ],
        });
        id++;
      });

      $(wrapper).on("click", ".delete", function (e) {
        e.preventDefault();
        const id = $(this).parent().parent().parent("div").prop("className");
        var index = self.state.ids.indexOf(parseInt(id));

        if (index !== -1) {
          var ids = [...self.state.ids];
          ids.splice(index, 1);
          self.setState({ ids: ids });
        }

        console.log(self.state.ids);
        $(this).parent().parent("div").remove();
      });
    });
  }

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
    console.log(event.target.name + " : " + event.target.value);
  };

  handleSubmit = (event) => {
    event.preventDefault();

    let self = this;
    const data = [];

    this.state.ids.forEach((id) => {
      const title = self.state["title" + id];
      const value = self.state["value" + id];

      data.push({
        key: title,
        value: value,
      });
    });

    var url = API_URL + "subrecord/add";

    var payload = {
      token: localStorage.getItem("token"),
      teamId: self.props.teamId,
      patientId: self.props.patientId,
      data: JSON.stringify(data),
    };

    axios
      .post(url, payload)
      .then(function (response) {
        window.location.href = "/team/" + self.props.teamId;
      })
      .catch(function (error) {
        console.log(error);

        ToastsStore.error(error.response.data.message);
      });
  };

  render() {
    return (
      <React.Fragment>
        <ToastsContainer
          store={ToastsStore}
          position={ToastsContainerPosition.TOP_RIGHT}
        />
        <div className="text-right">
          <button
            type="button"
            className="btn btn-danger mb-4"
            data-toggle="modal"
            data-target="#createModal"
          >
            Add
          </button>
        </div>
        <div
          className="modal fade"
          id="createModal"
          tabindex="-1"
          role="dialog"
          aria-labelledby="createModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="createModalLabel">
                  Medical Record Update
                </h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                <form className="form-signin" onSubmit={this.handleSubmit}>
                  <div className="container-add">
                    <div className="row">
                      <div className="col-5">
                        <div className="form-group">
                          <input
                            type="text"
                            className="form-control form-control-alternative"
                            id="title0"
                            name="title0"
                            value={this.state.title0}
                            onChange={this.handleChange}
                            placeholder="Title"
                            required
                          />
                        </div>
                      </div>
                      <div className="col-5">
                        <div className="form-group">
                          <input
                            type="text"
                            id="value-input0"
                            placeholder="Value"
                            name="value0"
                            value={this.state.value0}
                            onChange={this.handleChange}
                            className="form-control form-control-alternative"
                            required
                          />
                        </div>
                      </div>
                      <div className="col-2 text-center">
                        <button
                          type="button"
                          rel="tooltip"
                          id="button-new"
                          className="btn btn-success btn-icon btn-sm btn-simple h-32 text-center"
                          data-original-title=""
                          title=""
                          style={{ height: "32px", marginTop: "8px" }}
                        >
                          <h6 className="text-white h-32">+</h6>
                        </button>
                      </div>
                    </div>
                    {this.state.subComponents}
                  </div>
                  <div className="modal-footer">
                    <button
                      type="button"
                      className="btn btn-secondary"
                      data-dismiss="modal"
                    >
                      Close
                    </button>
                    <button type="submit" className="btn btn-primary">
                      Save
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default CreateSubRecord;

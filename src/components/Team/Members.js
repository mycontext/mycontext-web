import React, { Component } from "react";
import Moment from "moment";
import { mdiVodte, mdiOpenInNew } from "@mdi/js";
import Icon from "@mdi/react";
import axios from "axios";
import { API_URL } from "../../common";
import {
  ToastsContainer,
  ToastsStore,
  ToastsContainerPosition,
} from "react-toasts";

class Members extends Component {
  constructor(props) {
    super(props);
    this.state = {
      share: "",
      selectedMemberId:""
    };
  }
  componentDidMount() {

    console.log("props ",this.props)
    Moment.locale("en");
  }
  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
    const is=event.target.memberId;
    console.log (event)
  };
  handleSelect =(member)=>{
    this.setState({selectedMemberId: member.user._id});
    this.setState({share:member.share});
  }
  handleSubmit = (event) => {
    event.preventDefault();
    var self = this;

    self.setState({ loading: true });

    var payload = {
      token: localStorage.getItem("token"),
      memberId: this.state.selectedMemberId,
      share:this.state.share

    };
    var url = API_URL + "team/updateMemberShare";

    axios
      .post(url, payload)
      .then(function (response) {
        if (response.data.success) {
          window.location.reload();
          ToastsStore.success(response.data.message);
        } else {
          self.setState({ loading: false });
        ToastsStore.error(response.data.message);
        }
      })
      .catch(function (error) {
        self.setState({ loading: false });
        ToastsStore.error("Error occured when updating share!");
        
      });
    };

  render() {
    return this.props.members ? (
      <React.Fragment>
        <div className="table-responsive">
          <table className="table align-items-center table-flush mt-2">
            <thead className="thead-light">
              <tr>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Share</th> 
                <th scope="col">Created At</th>
                <th scope ="col"> Action</th>
              </tr>
            </thead>
            <tbody>
              {
                <tr>
                  <td>
                    <div className="d-flex align-items-center">
                      <span> {this.props.admin.name + " (Admin)"}</span>
                    </div>
                  </td>
                  <td>
                    <span>{this.props.admin.email}</span>
                  </td>
                  <td>
                  <span>{this.props.adminShare}</span>
                  </td>
                 
                    <td>
                    <div className="d-flex align-items-center">
                      <span>{Moment(this.props.createdAt).format("LL")}</span>
                    </div>
                  </td>
                </tr>
              }
              {this.props.members.map((member,index) => (
                <tr key={index}>
                  <td>
                    <div className="d-flex align-items-center">
                      <span> {member.user.name}</span>
                    </div>
                  </td>
                  <td>
                    <span>{member.user.email}</span>
                  </td>
                  <td>
                    <span>{member.share}</span>
                  </td>
                                    
                  <td>
                    <div className="d-flex align-items-center">
                      <span>{Moment(member.user.createdAt).format("LL")}</span>
                    </div>
                  </td>
                 
              
              
                  <td className="td-actions text-right">
              <div
                type="button"
                rel="tooltip"
                className="btn btn-info btn-icon btn-sm btn-simple"
               // onClick={edit(member)}
               // id="#Editbutton"
                data-toggle="modal"
                data-target="#editModal"
                onClick={() => this.handleSelect(member)}
                
              >
                <Icon
                  path={mdiOpenInNew}
                  title="Edit share"
                  size={0.5}
                  horizontal
                  vertical
                  rotate={180}
                  color="#fff"
                />
              </div>
              </td>
                 
                </tr>
                
              ))}
            </tbody>
          </table>
          <div
          className="modal fade"
          id="editModal"
          tabindex="-1"
          role="dialog"
          aria-labelledby="editModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="editModalLabel">
                  Edit share
                </h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                 
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                <form className="form-signin" onSubmit={this.handleSubmit}>
                  <div className="container-add">
                    <div className="form-group">
                      <input
                        type="number"
                        min="0"
                        max="100"
                        className="form-control form-control-alternative"
                        id="share"
                        name="share"
                        value={this.state.share}
                        onChange={this.handleChange}
                        placeholder="share"
                        required
                      />
                    </div>
                  </div>
                  <div className="modal-footer">
                    <button
                      type="button"
                      className="btn btn-secondary"
                      data-dismiss="modal"
                    >
                      Close
                    </button>
                    <button type="submit" className="btn btn-primary">
                      Save
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        </div>
      </React.Fragment>
    ) : (
      <h3>No members available</h3>
    );
  }
}
//document.getElementById('EditButton').setAttribute('data-toggle',"modal")

export default Members;

import React, { Component } from "react";
import axios from "axios";
import {
  ToastsContainer,
  ToastsStore,
  ToastsContainerPosition,
} from "react-toasts";
import { API_URL } from "../../common";

class AddMember extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: undefined,
      share: ""
    };
  }

  componentDidMount() {}

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleSubmit = (event) => {
    event.preventDefault();
    var self = this;

    self.setState({ loading: true });

    var payload = {
      token: localStorage.getItem("token"),
      emailAddress: this.state.email,
      share:this.state.share
    };

    var url = API_URL + "user/addTeamMemberByMailId ";

    axios
      .post(url, payload)
      .then(function (response) {
        if (response.data.success) {
          window.location.reload();
          ToastsStore.success(response.data.message);
        } else {
          self.setState({ loading: false });
          ToastsStore.error(response.data.message);
        }
      })
      .catch(function (error) {
        self.setState({ loading: false });
        ToastsStore.error(error.response.data.message);
        console.log(error);
      });
  };

  render() {
    return (
      <React.Fragment>
        <ToastsContainer
          store={ToastsStore}
          position={ToastsContainerPosition.TOP_RIGHT}
        />
        <div className="text-right">
          <button
            type="button"
            className="btn btn-danger mb-4"
            data-toggle="modal"
            data-target="#createModal"
          >
            Add
          </button>
        </div>
        <div
          className="modal fade"
          id="createModal"
          tabindex="-1"
          role="dialog"
          aria-labelledby="createModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="createModalLabel">
                  Add a member to Team
                </h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                <form className="form-signin" onSubmit={this.handleSubmit}>
                  <div className="container-add">
                    <div className="form-group">
                      <input
                        type="email"
                        className="form-control form-control-alternative"
                        id="email"
                        name="email"
                        value={this.state.email}
                        onChange={this.handleChange}
                        placeholder="Email"
                        required
                      />
                    </div>
                    <div className="form-group">
                      <input
                        type="number"
                        min="0"
                        max="100"
                        className="form-control form-control-alternative"
                        id="share"
                        name="share"
                        value={this.state.share}
                        onChange={this.handleChange}
                        placeholder="share"
                        required
                      />
                    </div>
                  </div>
                  <div className="modal-footer">
                    <button
                      type="button"
                      className="btn btn-secondary"
                      data-dismiss="modal"
                    >
                      Close
                    </button>
                    <button type="submit" className="btn btn-primary">
                      Save
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default AddMember;

import React, { Component } from "react";
import axios from "axios";
import { API_URL } from "../../common";
import {
  ToastsContainer,
  ToastsStore,
  ToastsContainerPosition,
} from "react-toasts";
import Record from "../ViewRecord/Record";
import Members from "../Team/Members";
import SubRecords from "../Team/SubRecords";
import AddMember from "./AddMember";
import Document from './Document';


class MyTeam extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: undefined,
      members: undefined,
      team: undefined,
      loading: true,
      index: undefined,
      email: "",
    };
  }

  componentDidMount() {
    document.title = "my Team";
    document.body.classList.add("white");
    var self = this;
    var url = API_URL + "record/getTeamRecord";
    var payload = {
      token: localStorage.getItem("token"),
    };

    axios
      .post(url, payload)
      .then((result) => {
        self.setState({
          data: result.data.data,
          team: result.data.team,
          members: result.data.team.members,
          loading: false,
        });
      })
      .catch((error) => {
        self.setState({ loading: false });
        ToastsStore.error(error.response.data.message);
      });
  }

  componentWillUnmount() {
    document.body.classList.remove("white");
  }

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {
    return (
      <React.Fragment>
        <ToastsContainer
          store={ToastsStore}
          position={ToastsContainerPosition.TOP_RIGHT}
        />
        <h3>{this.state.team ? this.state.team.admin.name + " Team" : ""}</h3>

        <nav className="nav nav-pills nav-justified">
          <a
            className="nav-item nav-link active"
            href="#nav-medical-record"
            data-toggle="tab"
            role="tab"
            aria-controls="nav-medical-record"
            aria-selected="true"
          >
            Medical Record
          </a>
          <a
            className="nav-item nav-link"
            href="#nav-team-members"
            data-toggle="tab"
            role="tab"
            aria-controls="nav-medical-members"
            aria-selected="true"
          >
            Members
          </a>
          <a
            className="nav-item nav-link"
            href="#nav-medical-history"
            data-toggle="tab"
            role="tab"
            aria-controls="nav-medical-history"
            aria-selected="true"
          >
            Medical History
          </a>
          <a
            className="nav-item nav-link"
            href="#nav-medical-documents"
            data-toggle="tab"
            role="tab"
            aria-controls="nav-medical-documents"
            aria-selected="true">
            Document
          </a>
        </nav>

        <div className="tab-content mt-4" id="nav-tabContent">
          <div
            className="tab-pane fade show active"
            id="nav-medical-record"
            role="tabpanel"
            aria-labelledby="nav-medical-record"
          >

            <Record data={this.state.data} />
          </div>
          <div
            className="tab-pane fade"
            id="nav-team-members"
            role="tabpanel"
            aria-labelledby="nav-team-members"
          >
            <AddMember />

            <Members
              admin={this.state.team ? this.state.team.admin : undefined}
              members={this.state.team ? this.state.team.members : undefined}
              adminShare={this.state.team ? this.state.team.adminShare : undefined}
            />
          </div>
          <div
            className="tab-pane fade"
            id="nav-medical-history"
            role="tabpanel"
            aria-labelledby="nav-medical-history"
          >
            <SubRecords
              teamId={this.props.teamId}
              patientId={this.state.data ? this.state.data.patient : undefined}
              
            />
          </div>
          <div
            className="tab-pane fade"
            id="nav-medical-documents"
            role="tabpanel"
            aria-labelledby="nav-medical-documents">
            <Document
              teamId={this.props.teamId}
              patientId={
                this.state.team ? this.state.team.admin._id : undefined
              }
              recordId={this.state.data ? this.state.data._id : undefined}
              documents={this.state.data ? this.state.data.files : []}
            />
          </div>
        </div>

      </React.Fragment>
    );
  }
}

export default MyTeam;

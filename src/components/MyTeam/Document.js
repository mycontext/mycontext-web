import React, { Component } from "react";
import axios from "axios";
import {
  ToastsContainer,
  ToastsStore,
  ToastsContainerPosition,
} from "react-toasts";
import Moment from "moment";
import { API_URL } from "../../common";
import { FilePicker } from "react-file-picker";
import Icon from "@mdi/react";
import { mdiDownload } from "@mdi/js";
import FileDownload from "js-file-download";

class Documents extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: undefined,
      loading: true,
    };
  }

  componentDidMount() {
    var self = this;
    Moment.locale("en");
  }

  /*uploadFile = (file) => {

    var self = this;
    var url = API_URL + 'record/addFileToRecord';

    var bodyFormData = new FormData();
    bodyFormData.set('fileName', file);
    bodyFormData.set('fileType', file.fileType);
    bodyFormData.set('teamId', this.props.teamId);
    bodyFormData.set('patientId', this.props.patientId);
    bodyFormData.set('recordId', this.props.recordId);

    axios({
      method: 'post',
      url: url,
      data: bodyFormData,
      headers: {
        'Content-Type': 'multipart/form-data',
        'x-access-token': localStorage.getItem('token'),
      },
    })
      .then(function (response) {
        console.log(response);
        if (response.data.success) {
          self.setState({ data: response.data.data, loading: false });
          console.log(self.state.data);
        } else {
          ToastsStore.error(response.data.message);
        }
      })
      .catch(function (error) {
        console.log(error);
        ToastsStore.error(error.response.data.message);
      });
  };*/

  handleDownload = (file) => {
    var self = this;
    var url = API_URL + "record/downloadFileFromRecord";

    const payload = {
      teamId: this.props.teamId,
      recordId: this.props.recordId,
      fileId: file.grid_id,
    };

    axios({
      method: "post",
      url: url,
      data: payload,
      headers: {
        "Content-Type": "application/json",
        "x-access-token": localStorage.getItem("token"),
      },
      responseType: "blob",
    })
      .then(function (response) {
        console.log(response);
        FileDownload(response.data, file.fileName);
      })
      .catch(function (error) {
        ToastsStore.error(error.response.data.message);
      });
  };

  render() {
    return (
      <React.Fragment>
        <ToastsContainer
          store={ToastsStore}
          position={ToastsContainerPosition.TOP_RIGHT}
        />

        <div className="table-responsive">
          <table className="table align-items-center table-flush mt-2">
            <thead className="thead-light">
              <tr>
                <th scope="col">Name</th>
                <th scope="col">Uploaded by</th>
                <th scope="col">Type</th>
                <th scope="col">Uploaded on</th>
                <th scope="col"> Action</th>
              </tr>
            </thead>
            <tbody>
              {this.props.documents.map((document, index) => (
                <tr key={index}>
                  <td>{document.fileName}</td>
                  <td>{document.uploaded_by}</td>
                  <td>{document.mimeType}</td>
                  <td>{Moment(document.uploaded_on).format("LL")}</td>
                  <td>
                    <div
                      type="button"
                      rel="tooltip"
                      className="btn btn-info btn-icon btn-sm btn-simple"
                      onClick={() => this.handleDownload(document)}
                    >
                      <Icon
                        path={mdiDownload}
                        title="Edit share"
                        size={0.5}
                        horizontal
                        vertical
                        rotate={180}
                        color="#fff"
                      />
                    </div>
                  </td>
                </tr>
              ))}
              {/* {this.props.documents.map((member, index) => (
                <tr key={index}>
                  </tr>))} */}
            </tbody>
          </table>
        </div>
      </React.Fragment>
    );
  }
}

export default Documents;

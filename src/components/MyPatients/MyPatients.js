import React, { Component } from "react";
import axios from "axios";
import { ToastsStore } from "react-toasts";
import $ from "jquery";
import { API_URL } from "../../common";
import { mdiVote, mdiOpenInNew } from "@mdi/js";
import Icon from "@mdi/react";

class MyPatients extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loading: true,
      index: undefined,
      email: "",
      users: []
    };
  }

  componentDidMount() {
    document.title = "Records";
    document.body.classList.add("white");
    this.getUsersList();
    var self = this;

    var url = API_URL + "record/listRecords";

    var payload = {
      token: localStorage.getItem("token"),
      from: 0,
      size: 100
    };

    axios
      .post(url, payload)
      .then(result => {
        console.log();
        console.log(result.data);
        self.setState({ data: result.data.data, loading: false });
      })
      .catch(error => {
        console.log(error);
      });

    // axios
    //   .get(
    //     API_URL + `record/my-records?userId=${localStorage.getItem("user_id")}`,
    //     {
    //       headers: {
    //         "x-access-token": localStorage.getItem("access-token")
    //       }
    //     }
    //   )
    //   .then(function(response) {
    //     console.log(response.data.data);
    //     if (response.data.success) {
    //       self.setState({ data: response.data.data, loading: false });
    //       // axios
    //       //   .post(url, payload)
    //       //   .then(function (response) {
    //       //     if (response.data.success) {
    //       //       self.setState(prevState => ({
    //       //         data: [prevState.data, ...response.data.data],
    //       //         loading: false
    //       //       }));
    //       //       console.log(self.state.data);
    //       //     } else {
    //       //       self.setState({
    //       //         loading: false
    //       //       })
    //       //       // ToastsStore.error(response.data.message);
    //       //     }
    //       //   })
    //       //   .catch(function (error) {
    //       //     ToastsStore.error(error.response.data.message);
    //       //   });
    //     } else {
    //       ToastsStore.error(response.data.message);
    //     }
    //   })
    //   .catch(function(error) {
    //     ToastsStore.error(error.response.data.message);
    //   });
  }

  getUsersList = () => {
    var self = this;

    var url = API_URL + "user/list-users";

    var payload = {
      token: localStorage.getItem("access-token")
    };

    axios
      .post(url, payload)
      .then(function(response) {
        if (response.data.success) {
          let myUserId = localStorage.getItem("user_id");
          let data = response.data.data.filter(x => x._id !== myUserId);
          self.setState({ users: data, loading: false });
        } else {
          ToastsStore.error(response.data.message);
        }
      })
      .catch(function(error) {
        ToastsStore.error(error.response.data.message);
      });
  };

  componentWillUnmount() {
    document.body.classList.remove("white");
  }

  viewDetails = (event, id) => {
    window.open("/view/" + id, "_blank");
  };

  editDetails = (event, id) => {
    window.open("/edit/" + id, "_blank");
  };

  handleDelete = (event, index) => {
    this.setState({ index: index });
    $("#deleteModal").modal("show");
  };

  deleteRecord = event => {
    $("#deleteModal").modal("hide");
    const record = this.state.data[this.state.index];
    this.setState({
      loading: true
    });

    var url = API_URL + "record/deleteRecord";
    var self = this;

    var payload = {
      token: localStorage.getItem("access-token"),
      id: record._id
    };

    axios
      .post(url, payload)
      .then(function(response) {
        if (response.data.success) {
          self.setState({
            data: self.state.data.filter((_, i) => i !== self.state.index)
          });

          console.log(self.state.data);

          ToastsStore.success(response.data.message);
          self.setState({
            loading: false,
            index: undefined
          });
        } else {
          ToastsStore.error(response.data.message);
        }
      })
      .catch(function(error) {
        self.setState({
          loading: false,
          index: undefined
        });
        ToastsStore.error(error.response.data.message);
      });
  };

  handleTransfer = (event, index) => {
    this.setState({ index: index, email: "" });
    $("#transferModal").modal("show");
  };

  transferRecord = event => {
    if (!this.state.email) {
      ToastsStore.error("Select user");
    } else {
      $("#transferModal").modal("hide");
      const record = this.state.data[this.state.index];
      this.setState({
        loading: true
      });

      var url = API_URL + "record/changeOwnership";
      var self = this;
      let myUserId = localStorage.getItem("user_id");
      var payload = {
        token: localStorage.getItem("access-token"),
        medicalrecordId: record._id,
        newOwnerMail: this.state.email,
        newOwnerId: this.state.users.find(x => x.email === this.state.email)
          ._id,
        currentUserId: myUserId
      };

      axios
        .post(url, payload)
        .then(function(response) {
          if (response.data.success) {
            self.setState({
              data: self.state.data.filter((_, i) => i !== self.state.index)
            });

            ToastsStore.success(response.data.message);
          } else {
            ToastsStore.error(response.data.message);
          }

          self.setState({
            loading: false,
            email: "",
            index: undefined
          });
        })
        .catch(function(error) {
          self.setState({
            loading: false,
            email: "",
            index: undefined
          });
          ToastsStore.error(error.response.data.message);
        });
    }
  };

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {
    return (
      <React.Fragment>
        <div className="table-responsive">
          <table className="table align-items-center table-flush mt-2">
            <thead className="thead-light">
              <tr>
                <th scope="col">Patient Team </th>
                <th scope="col">Name</th>
                <th scope="col">Type</th>
                <th scope="col">Gender</th>
                <th scope="col">Year Of Birth</th>
                <th scope="col" />
              </tr>
            </thead>
            <tbody>
              {this.state.data.map((record, index) => (
                <tr key={index}>
                  <td>
                    <div className="d-flex align-items-center">
                    {         record.user? <span > {record.user.name}</span>
        : <span > </span>
      }
                     
       
                    </div>
                  </td>
                  <td>
                    <span className="badge badge-info mr-4">
                      {record.price}
                    </span>
                  </td>
                  <td>
                    <div className="d-flex align-items-center">
                      <span> {record.cancer_type}</span>
                    </div>
                  </td>
                  <td>
                    <div className="d-flex align-items-center">
                      <span> {record.gender}</span>
                    </div>
                  </td>
                  <td>
                    <div className="d-flex align-items-center">
                      <span> {record.year_of_birth}</span>
                    </div>
                  </td>
                  <td className="td-actions text-right">
                    <a
                      type="button"
                      rel="tooltip"
                      className="btn btn-info btn-icon btn-sm btn-simple"
                      href={"/view/" + record._id}
                    >
                      <Icon
                        path={mdiOpenInNew}
                        title="User Profile"
                        size={0.5}
                        horizontal
                        vertical
                        rotate={180}
                        color="#fff"
                      />
                    </a>
                    <button
                      type="button"
                      rel="tooltip"
                      className="btn btn-danger btn-icon btn-sm btn-simple"
                      data-original-title=""
                      title=""
                    >
                      <Icon
                        path={mdiVote}
                        title="User Profile"
                        size={0.5}
                        horizontal
                        vertical
                        rotate={180}
                        color="#fff"
                      />
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </React.Fragment>
    );
  }
}

export default MyPatients;

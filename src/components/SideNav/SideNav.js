import React, { Component } from "react";
import "./SideNav.css";
import Icon from "@mdi/react";
import {
  mdiFileDocument,
  mdiAccountGroup,
  mdiMonitor,
  mdiGavel,
  mdiWallet,
} from "@mdi/js";

import Dashboard from "../Dashboard/Dashboard";
import Graphs from "../Graph/Graphs";
import Records from "../Records/Records";
import ViewRecord from "../ViewRecord/ViewRecord";
import Teams from "../Teams/Teams";
import Team from "../Team/Team";
import MyTeam from "../MyTeam/MyTeam";
import MyPatients from "../MyPatients/MyPatients";
import Bids from "../Bids/Bids";
import BuyerBids from "../BuyerBids/BuyerBids";
import RecordsForBids from "../Records/RecordsForBids";
import AllBids from "../Bids/AllBids";
import Wallet from "../Wallet/Wallet";
const logo = require("../../img/logo.png");

class SideNav extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user_type: localStorage.getItem("user_type"),
    };
  }

  isActive = (value) => {
    return "nav-link " + (value === this.props.title ? "active" : "nav-link");
  };

  handleClick = (event, type) => {
    event.preventDefault();
    this.props.handleNavigation(type);

    var component = <Dashboard />;
    var header = undefined;

    switch (type) {
      case "":
      case "dashboard":
        component = <Dashboard />;
        header = <Graphs />;
        break;
      case "records":
        component = <Records />;
        break;
        case "wallet":
          component = <Wallet />;
          break;
      case "teams":
        component = <Teams />;
        break;
      case "my-patients":
        component = <MyPatients />;
        break;
      case "my-team":
        component = <MyTeam />;
        break;
      case "bids":
        component = <Bids />;
        break;
      case "buyerbids":
        component = <BuyerBids />;
        break;
      case "records-for-bids":
        component = <RecordsForBids />;
      case "all-bids":
        component = <AllBids />;
        break;
      default:
        if (type.indexOf("view") !== -1) {
          component = <ViewRecord />;
        } else {
          component = <Team />;
        }
    }

    this.props.setComponent(component, type, header);
  };

  handleLogout = (event) => {
    event.preventDefault();
    localStorage.removeItem("token");
    localStorage.removeItem("refreshToken");
    localStorage.removeItem("name");
    this.props.setLogout();
  };

  render() {
    return (
      <nav
        className="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white"
        id="sidenav-main"
      >
        <div className="container-fluid">
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#sidenav-collapse-main"
            aria-controls="sidenav-main"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon" />
          </button>
          <a className="navbar-brand pt-0" href="/">
            <img
              src={logo}
              height="500px"
              className="navbar-brand-img h-100"
              alt="..."
            />
            <p>MyContext</p>
          </a>
          <ul className="nav align-items-center d-md-none">
            <li className="nav-item dropdown">
              <a
                className="nav-link"
                href="/"
                role="button"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                <div className="media align-items-center">
                  <span className="avatar avatar-sm rounded-circle">
                    <img src="../assets/img/theme/team-1-800x800.jpg" alt="" />
                  </span>
                </div>
              </a>
              <div className="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                <div className=" dropdown-header noti-title">
                  <h6 className="text-overflow m-0">Welcome!</h6>
                </div>
                <a href="./examples/profile.html" className="dropdown-item">
                  <i className="ni ni-single-02" />
                  <span>My profile</span>
                </a>
                <div className="dropdown-divider" />
                <a href="#!" className="dropdown-item">
                  <i className="ni ni-user-run" />
                  <span>Logout</span>
                </a>
              </div>
            </li>
          </ul>
          <div className="collapse navbar-collapse" id="sidenav-collapse-main">
            <div className="navbar-collapse-header d-md-none">
              <div className="row">
                <div className="col-6 collapse-brand">
                  <a href="../index.html">
                    <img src="../assets/img/brand/blue.png" alt="" />
                  </a>
                </div>
                <div className="col-6 collapse-close">
                  <button
                    type="button"
                    className="navbar-toggler"
                    data-toggle="collapse"
                    data-target="#sidenav-collapse-main"
                    aria-controls="sidenav-main"
                    aria-expanded="false"
                    aria-label="Toggle sidenav"
                  >
                    <span />
                    <span />
                  </button>
                </div>
              </div>
            </div>
            <h6 className="navbar-heading text-muted">Administration</h6>
            <ul className="navbar-nav mb-md-3">
              <li className="nav-item">
                <a
                  className={this.isActive("dashboard")}
                  onClick={(event) => {
                    this.handleClick(event, "dashboard");
                  }}
                  href="/dashboard"
                >
                  <Icon
                    path={mdiMonitor}
                    title="dashboard"
                    size={0.9}
                    horizontal
                    vertical
                    rotate={180}
                    color="#e74c3c"
                    className="mr-2"
                  />
                  Dashboard
                </a>
              </li>
              <li
                className={
                  this.state.user_type !== "Patient" ? "nav-item" : "d-none"
                }
              >
                <a
                  className={this.isActive("records")}
                  onClick={(event) => {
                    this.handleClick(event, "records");
                  }}
                  href="/"
                >
                  <Icon
                    path={mdiFileDocument}
                    title="records"
                    size={0.9}
                    horizontal
                    vertical
                    rotate={180}
                    color="#1abc9c"
                    className="mr-2"
                  />
                  Records
                </a>
              </li>
              <li
                className={
                  this.state.user_type === "Pharmaceutical Company" ||
                  this.state.user_type === "Pathology Laboratory" ||
                  this.state.user_type === "Insurance Company"
                    ? "nav-item"
                    : "d-none"
                }
              >
                <a
                  className={this.isActive("all-bids")}
                  onClick={(event) => {
                    this.handleClick(event, "all-bids");
                  }}
                  href="/"
                >
                  <Icon
                    path={mdiFileDocument}
                    title="allbids"
                    size={0.9}
                    horizontal
                    vertical
                    rotate={180}
                    color="#1abc9c"
                    className="mr-2"
                  />
                  Bids history
                </a>
              </li>
              {/* <li
                className={
                  this.state.user_type === "Doctor"|| this.state.user_type === "Hospital" || this.state.user_type === "Pathology Laboratory"   ||this.state.user_type === "Pathologist" ? "nav-item" : "d-none"
                }
              >
                <a
                  className={this.isActive("my-patients")}
                  onClick={event => {
                    this.handleClick(event, "my-patients");
                  }}
                  href="/"
                >
                  <Icon
                    path={mdiFileDocument}
                    title="my-patients"
                    size={0.9}
                    horizontal
                    vertical
                    rotate={180}
                    color="#f39c12"
                    className="mr-2"
                  />
                  My Patients
                </a>
              </li> */}

              <li
                className={
                  this.state.user_type === "Doctor" ? "nav-item" : "d-none"
                }
              >
                <a
                  className={this.isActive("teams")}
                  onClick={(event) => {
                    this.handleClick(event, "teams");
                  }}
                  href="/"
                >
                  <Icon
                    path={mdiAccountGroup}
                    title="teams"
                    size={0.9}
                    horizontal
                    vertical
                    rotate={180}
                    color="#f39c12"
                    className="mr-2"
                  />
                  teams
                </a>
              </li>
              {/* <li
                className={
                  this.state.user_type === "Patient" ? "nav-item" : "d-none"
                }
              >
                <a
                  className={this.isActive("my-records")}
                  onClick={event => {
                    this.handleClick(event, "my-records");
                  }}
                  href="/"
                >
                   <Icon
                    path={mdiFileDocument}
                    title="profile"
                    size={0.9}
                    horizontal
                    vertical
                    rotate={180}
                    color="#1abc9c"
                    className="mr-2"
                  />
                 Medical profile
                </a>
              </li> */}
              <li
                className={
                  this.state.user_type === "Patient" ? "nav-item" : "d-none"
                }
              >
                <a
                  className={this.isActive("my-team")}
                  onClick={(event) => {
                    this.handleClick(event, "my-team");
                  }}
                  href="/"
                >
                  <Icon
                    path={mdiFileDocument}
                    title="my-team"
                    size={0.9}
                    horizontal
                    vertical
                    rotate={180}
                    color="#f39c12"
                    className="mr-2"
                  />
                  Medical Team
                </a>
              </li>
              <li
                className={
                  this.state.user_type === "Patient" ||
                  this.state.user_type == "Doctor"
                    ? "nav-item"
                    : "d-none"
                }
              >
                <a
                  className={this.isActive("wallet")}
                  onClick={(event) => {
                    this.handleClick(event, "wallet");
                  }}
                  href="/"
                >
                  <Icon
                    path={mdiWallet}
                    title="wallet"
                    size={0.9}
                    horizontal
                    vertical
                    rotate={180}
                    color="#1abc9c"
                    className="mr-2"
                  />
                  wallet
                </a>
              </li>
 <li
                className={
                  this.state.user_type === "Patient" ||
                  this.state.user_type == "Doctor"
                    ? "nav-item"
                    : "d-none"
                }
              >
                <a
                  className={this.isActive("bids")}
                  onClick={(event) => {
                    this.handleClick(event, "bids");
                  }}
                  href="/"
                >
                  <Icon
                    path={mdiGavel}
                    title="bids"
                    size={0.9}
                    horizontal
                    vertical
                    rotate={180}
                    color="#1abc9c"
                    className="mr-2"
                  />
                  Bids
                </a>
              </li>

            
              <li
                className={
                  this.state.user_type === "Pharmaceutical Company" ||
                  this.state.user_type === "Pathology Laboratory" ||
                  this.state.user_type === "Insurance Company"
                    ? "nav-item"
                    : "d-none"
                }
              >
                <a
                  className={this.isActive("buyerbids")}
                  onClick={(event) => {
                    this.handleClick(event, "buyerbids");
                  }}
                  href="/"
                >
                  <Icon
                    path={mdiGavel}
                    title="My bids"
                    size={0.9}
                    horizontal
                    vertical
                    rotate={180}
                    color="#1abc9c"
                    className="mr-2"
                  />
                  My Bids
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    );
  }
}

export default SideNav;
